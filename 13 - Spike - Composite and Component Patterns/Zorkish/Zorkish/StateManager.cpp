#include "StateManager.h"

StateManager::StateManager()
{
	isRunning = true;

	states[StateType::MAIN_MENU] = new MainMenuState();
	states[StateType::ABOUT] = new AboutState();
	states[StateType::HELP] = new HelpState();
	states[StateType::SELECTADVENTURE] = new SelectAdventureState();
	states[StateType::GAMEPLAY] = new GameplayState();
	states[StateType::NEWHIGHSCORE] = new NewHighScoreState();
	states[StateType::VIEWHALLOFFAME] = new ViewHallOfFameState();

	currentState = states[StateType::MAIN_MENU];
}

void StateManager::update(string input)
{
	StateType newState = currentState->update(input);

	if (newState == StateType::GAMEPLAY)
	{
		//create a new gameplay state to assign values to
		GameplayState* newgame =  new GameplayState();
		SelectAdventureState adventureselect;
		if (newgame->WorldGen(adventureselect.WorldSelect(input)))
			states[StateType::GAMEPLAY] = newgame;
		else
			newState = StateType::NONE;
	}
	if (newState == StateType::QUIT)
		isRunning = false;
	else if (newState != StateType::NONE)
		currentState = states[newState];
}

void StateManager::render()
{
	currentState->render();
}

