#ifndef STATESINCLUDED
#define STATESINCLUDED
#include "States.h"
#endif

AboutState::AboutState()
{
}

StateType AboutState::update(string input)
{
	return StateType::MAIN_MENU;
}

void AboutState::render()
{
	cout << "Written by: Jordan Heath" << endl;
	cout << "Press Enter to return to the Main Menu" << endl;
}