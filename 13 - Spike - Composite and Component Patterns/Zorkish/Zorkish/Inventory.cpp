#include "Inventory.h"

Inventory::Inventory() 
{
	items = vector<Item*>();
}

Inventory::~Inventory()
{
	items.clear();
}

void Inventory::print()
{
	string result = "";

	for (Item* item : items)
		result += item->getName() + ", ";

	if (result != "")
	{
		result.pop_back();
		result.pop_back();

		cout << result << endl;
	}
	else
		cout << "nothing" << endl;
	
}

bool Inventory::empty()
{
	return items.empty();
}

void Inventory::deposit(Item* item)
{
	for (size_t i = 0; i < items.size(); i++)
	{
		if (items[i]->getID() == item->getID())
		{
			int quantity = items[i]->getQuantity() + item->getQuantity();
			items[i]->setQuantity(quantity);
			delete item;
			break;
		}
		else if (i == items.size() - 1)
		{
			items.push_back(item);
			break;
		}
	}
	if (items.size() == 0)
		items.push_back(item);
}

Item* Inventory::withdraw(string ID)
{
	Item* result = nullptr;

	// find item
	for (size_t i = 0; i < items.size(); i++)
	{
		if (items[i]->getID() == ID)
		{
			result = items[i];
			vector<Item*>::iterator it;
			it = items.begin();
			for (size_t j = 0; j < i; j++)
				it++;
			items.erase(it);
			break;
		}
	}

	return result;
}

Item* Inventory::finditem(string ID)
{
	for (Item* i : items)
	{
		if (i->getID() == ID)
			return i;
	}

	return nullptr;
}