#pragma once

#include <string>
#include <algorithm>
#include <iostream>
#include <cctype>

#include "ComponentManager.h"

using namespace std;

class Entity
{
public:
	string name;
	string description;
	ComponentManager components;

	Entity();
	Entity(string newname, string newdescription, vector<string> componentinstructions);

	string getID();
	string getName();
	string getDescription();
};