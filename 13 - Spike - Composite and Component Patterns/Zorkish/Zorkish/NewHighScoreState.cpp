#ifndef STATESINCLUDED
#define STATESINCLUDED
#include "States.h"
#endif

NewHighScoreState::NewHighScoreState()
{
}

StateType NewHighScoreState::update(string input)
{
	return StateType::VIEWHALLOFFAME;
}

void NewHighScoreState::render()
{
	cout << "Congratulations!" << endl;

	cout << "You have made it to the Zorkish Hall Of Fame" << endl;

	cout << "\tAdventure: " << "[the adventure world played]" << endl;
	cout << "\tScore: " << "[the players score]" << endl;
	cout << "\tMoves: " << "[number of moves player made]" << endl;

	cout << "Please type your name and press enter:" << endl;
	cout << ":> ";
}