#ifndef STATESINCLUDED
#define STATESINCLUDED
#include "States.h"
#endif

GameplayState::GameplayState()
{
	manager = CommandManager();
	player = Player();
	graph = Graph();
	bool started = false;
}

StateType GameplayState::update(string input)
{
	manager.execute(input, &graph);


	if (input == "takegoldcoin")
	{
		//initialise the gold coin
		Coin* coin = new Coin;
		graph.deposit(coin);

		//the command system triggers a search in the worldInventory to find the gold coin
		Item* foundItem;
		foundItem = graph.withdraw("gold coin");

		//if something is found, it adds it to player inventory
		if (foundItem != nullptr)
		{
			cout << "you picked up a " << foundItem->getName() << endl;
			player.deposit(foundItem);
		}

		//FIGURE OUT HOW TO PREVENT SCOPING PROBLEM
	}
	else if (input == "dropgoldcoins")
	{
		Item* foundItem;
		foundItem = player.withdraw("gold coin");

		//if something is found, it adds it to world inventory
		if (foundItem != nullptr)
		{
			cout << "you dropped " << foundItem->getName() << endl;
			graph.deposit(foundItem);
		}
	}
	else if (input == "quit")
	{
		cout << "Your adventure has ended without fame or fortune." << endl;
		return StateType::MAIN_MENU;
	}
	else if (input == "hiscore")
	{
		cout << "You have entered the magic word and will now see the �New High Score� screen." << endl;
		return StateType::NEWHIGHSCORE;
	}
	return StateType::NONE;
}

void GameplayState::render()
{
	cout << endl;
	if (!started)
	{
		//cout << "Welcome to Zorkish: Void World" << endl;
		//cout << "This world is simple and pointless. Used it to test Zorkish phase 1 spec." << endl;
		graph.look();
		started = true;
	}

	cout << graph.directions() << endl;
	cout << "What would you like to do?" << endl;
	
	cout << ":>";
}

bool GameplayState::WorldGen(string world)
{
	try
	{
		ifstream file(world);
		if (file.is_open())
		{
			file.close();

			graph = Graph(&player, world);

			return true;
		}
		else
		{
			cout << "Error: file did not open" << endl;
		}
	}
	catch (exception e)
	{
		cout << e.what();
	}
	return false;
}
