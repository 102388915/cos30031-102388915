#include "Room.h"


vector<string> Room::SplitString(string s, string delimiter)
{
	vector<string> result;
	size_t pos = 0;
	string token;
	while ((pos = s.find(delimiter)) != string::npos) {
		token = s.substr(0, pos);
		result.push_back(token);
		s.erase(0, pos + delimiter.length());
	}
	result.push_back(s);
	return result;
}

Room::Room()
{
	Name = "";
}

Room::Room(string name, string entitystring)
{
	Name = name;

	//entitystring format = "item"rock,its heavy,1,damage3"|entity"troll,rather fierce,health4"

	vector<string> instructions = SplitString(entitystring, "|");
	//entitystring format = "item,rock,its heavy,1,damage3" "entity,troll,rather fierce,health4,damage1"

	for (string entity : instructions)
	{
		vector<string> parameters = SplitString(entity, ",");
		//parameters format = "item" "rock" "its heavy" "1" "damage3"
		if (parameters[0] == "item")
		{
			vector<string> split_lo(parameters.begin(), parameters.begin() + 4);
			vector<string> split_hi(parameters.begin() + 4, parameters.end());
			Inventory.deposit(new Item(parameters[1], parameters[2], stoi(parameters[3]), split_hi));
		}
		//parameters format = "entity" "troll" "rather fierce" "health4" "damage1"
		else if (parameters[0] == "entity")
		{
			vector<string> split_lo(parameters.begin(), parameters.begin() + 3);
			vector<string> split_hi(parameters.begin() + 3, parameters.end());
			entities.push_back(new Entity(parameters[1], parameters[2], split_hi));
		}
	}
}

void Room::PrintInventory()
{
	Inventory.print();
}

void Room::deposit(Item* item)
{
	Inventory.deposit(item);
}

Item* Room::withdraw(string ID)
{
	return Inventory.withdraw(ID);
}

Item* Room::finditem(string ID)
{
	return Inventory.finditem(ID);
}

Entity* Room::findentity(string ID)
{
	for (Entity* e : entities)
	{
		if (e->getID() == ID)
			return e;
	}
	return nullptr;
}
