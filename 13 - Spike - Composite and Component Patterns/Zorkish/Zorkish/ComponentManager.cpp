#include "ComponentManager.h"

ComponentManager::ComponentManager()
{
}

void ComponentManager::AddComponent(string componentinstructions)
{
    if (componentinstructions.find("health") != string::npos)
    {
        componentinstructions.erase(0, 6);
        components.push_back(new HealthComponent(stoi(componentinstructions)));
    }
    if (componentinstructions.find("damage") != string::npos)
    {
        componentinstructions.erase(0, 6);
        components.push_back(new DamageComponent(stoi(componentinstructions)));
    }
}

void ComponentManager::printAttributes()
{
    for (Component* c : components)
    {
        cout << c->description << endl;
    }
}

bool ComponentManager::Attack(int change)
{
    for (Component* c : components)
    {
        if (c->Update(vector<string> {"SubtractHealth", to_string(change)}) != "")
            return true;
    }
    return false;
}

bool ComponentManager::Heal(int change)
{
    for (Component* c : components)
    {
        if (c->Update(vector<string> {"AddHealth", to_string(change)}) != "")
            return true;
    }
    return false;
}

int ComponentManager::Damage()
{
    string result;
    for (Component* c : components)
    {
        result = c->Update(vector<string> {"Damage"});
        if (result != "")
            return stoi(result);
    }
    return NULL;
}
