#pragma once

#include <map>

#include "components.h"

using namespace std;

class ComponentManager
{
private:
	vector<Component*> components;

public:
	ComponentManager();
	void AddComponent(string componentinstructions);
	void printAttributes();

	//possible components
	bool Attack(int change);
	bool Heal(int change);
	int Damage();
};