#pragma once

#ifndef MAPINCLUDED
#define MAPINCLUDED
#include <map>
#endif

#include <fstream>

#include "Room.h"
#include "Player.h"

//using namespace std;

class Graph
{
private:
	map<int, map<int, Room>> M;

	vector<string> SplitString(string s, string delimiter);
public:
	Player* player;

	Graph();
	Graph(Player* player, string filelocation);
	void north();
	void south();
	void east();
	void west();
	void look();
	void debug();
	string directions();
	Room* currentRoom();

	//inventory functions
	void deposit(Item* item);
	Item* withdraw(string ID);
	Item* finditem(string ID);
	Entity* findentity(string ID);
};