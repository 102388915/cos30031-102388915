#include "Entities.h"

Entity::Entity()
{
    name = "";
    description = "";
}

Entity::Entity(string newname, string newdescription, vector<string> componentinstructions)
{
    //process instructions to build name, description and attributes
    name = newname;
    description = newdescription;

    for (string c : componentinstructions)
        components.AddComponent(c);
}

string Entity::getID()
{
	//same as get name without capitals or quantity specifier
	string id = name;

	std::transform(id.begin(), id.end(), id.begin(),
		[](unsigned char c) { return std::tolower(c); });

	return id;
}

string Entity::getName()
{
	return name;
}

string Entity::getDescription()
{
	if (description != "")
		return description;
	return "There is no description for this object";
}