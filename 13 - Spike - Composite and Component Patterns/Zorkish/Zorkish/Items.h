#pragma once

#include "Entities.h"

using namespace std;

class Item : public Entity
{
public:
	int quantity;

	Item();
	Item(string newname, string newdescription, int newquantity, vector<string> components);

	string getName();
	int getQuantity();
	void setQuantity(int newQuantity);

	void lookAt();
	void lookIn();
	void open();
	void close();
};

class Bag : public Item
{
private:
	bool opened;
	bool looted;

public:
	Bag();

	void lookAt();
	void lookIn();
	void open();
	void close();
};

class Coin : public Item
{
public:
	Coin();

	void lookAt();
	void lookIn();
	void open();
	void close();
};