#pragma once

#include "States.h"

//using namespace std;

class StateManager
{
private:
	map <StateType, State*> states;
	State* currentState;

public:
	StateManager();
	void update(string input);
	void render();

	bool isRunning;
};

