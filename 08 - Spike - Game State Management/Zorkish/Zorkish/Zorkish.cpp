#include <iostream>
#include "statemanager.h"

int main()
{
    StateManager manager;
    string input;

    while (manager.isRunning)
    {
        manager.render();

        cin >> input;

        manager.update(input);
    }
}