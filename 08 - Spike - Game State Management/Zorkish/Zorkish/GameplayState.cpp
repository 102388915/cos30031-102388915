#include "states.h"

GameplayState::GameplayState()
{
}

StateType GameplayState::update(string input)
{
	if (input == "quit")
	{
		cout << "Your adventure has ended without fame or fortune." << endl;
		return StateType::MAIN_MENU;
	}
	else if (input == "hiscore")
	{
		cout << "You have entered the magic word and will now see the �New High Score� screen." << endl;
		return StateType::NEWHIGHSCORE;
	}
	return StateType::NONE;
}

void GameplayState::render()
{
	cout << "Welcome to Zorkish: Void World" << endl;
	cout << "This world is simple and pointless. Used it to test Zorkish phase 1 spec." << endl;
	cout << ":>";
}