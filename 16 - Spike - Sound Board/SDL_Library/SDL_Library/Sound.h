#pragma once
#include <SDL.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <map>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Sound {
public:
	const char* path;

	SDL_AudioSpec wavSpec;
	Uint32 wavLength;
	Uint8* wavBuffer;
	SDL_AudioDeviceID deviceid;

	bool paused;

	Sound();
	Sound(const char* Path);
	~Sound();

	bool init();
	void toggle();
	void play();
};