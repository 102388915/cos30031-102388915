#include "sound.h"

//using namespace std;

const int SCREEN_WIDTH = 600;
const int SCREEN_HEIGHT = 800;

bool running = true;

bool init();
void update();
void togglebg();
void playsound(int track);
void close();

SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;

const char* musicpath = "pachelbels-canon-arranged.wav";
const char* sfx1path = "alarm_clock.wav";
const char* sfx2path = "beep_short.wav";
const char* sfx3path = "bugle_tune.wav";

Sound music("pachelbels-canon-arranged.wav");

vector<Sound*> soundeffects;


bool init()
{
    bool result = true;

    if (SDL_Init(SDL_INIT_AUDIO) < 0)
    {
        printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
        result = false;
    }
    else
    {
        window = SDL_CreateWindow("SDL Test",
            SDL_WINDOWPOS_CENTERED,
            SDL_WINDOWPOS_CENTERED,
            SCREEN_WIDTH, SCREEN_HEIGHT,
            SDL_WINDOW_SHOWN);

        if (window == NULL)
        {
            printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
            result = false;
        }
        else
        {
            //Get window surface
            renderer = SDL_CreateRenderer(window, -1, 0);


            // load WAV file
            if (music.init())
                result = true;
        }
    }

    return result;
}

void update()
{
    //check input and trigger randmomise or quit
    // Source: https://www.youtube.com/watch?v=Gjhvz4banWA
    SDL_Event event;
    const Uint8* keystates = SDL_GetKeyboardState(NULL);
    while (SDL_PollEvent(&event))
    {
        if (event.type == SDL_QUIT)
            running = false;
        if (keystates[SDL_SCANCODE_0] || keystates[SDL_SCANCODE_KP_0])
            togglebg();
        if (keystates[SDL_SCANCODE_1] || keystates[SDL_SCANCODE_KP_1])
            playsound(1);
        if (keystates[SDL_SCANCODE_2] || keystates[SDL_SCANCODE_KP_2])
            playsound(2);
        if (keystates[SDL_SCANCODE_3] || keystates[SDL_SCANCODE_KP_3])
            playsound(3);
    }

    if (soundeffects.size() > 9)
    {
        for (size_t i = soundeffects.size() - 1; i > 0; i--)
            delete soundeffects[i];
        soundeffects.clear();
    }
}

void togglebg()
{
    music.toggle();
    if (music.paused)
        cout << "music paused" << endl;
    else
        cout << "music played" << endl;
    SDL_Delay(200);
}

void playsound(int track)
{
    if (track == 1)
    {
        soundeffects.push_back(new Sound(sfx1path));
        if (soundeffects[soundeffects.size() - 1]->init())
            soundeffects[soundeffects.size() - 1]->play();
    }
    if (track == 2)
    {
        soundeffects.push_back(new Sound(sfx2path));
        if (soundeffects[soundeffects.size() - 1]->init())
            soundeffects[soundeffects.size() - 1]->play();
    }
    if (track == 3 )
    {
        soundeffects.push_back(new Sound(sfx3path));
        if (soundeffects[soundeffects.size() - 1]->init())
            soundeffects[soundeffects.size() - 1]->play();
    }

    SDL_Delay(200);
}

void close()
{
    //Destroy window
    delete &music;
    for (size_t i = soundeffects.size() - 1; i > 0; i--)
        delete soundeffects[i];
    soundeffects.clear();

    //Quit SDL subsystems
    SDL_Quit();
}

int main(int argc, char* args[])
{
    //Start up SDL and create window
    if (!init())
    {
        printf("Failed to initialize!\n");
        return 1;
    }
    cout << "initialised" << endl;

    //Main Loop
    while (running)
    {
        update();
    }

    //Free resources and close SDL
    close();

    return 0;
}