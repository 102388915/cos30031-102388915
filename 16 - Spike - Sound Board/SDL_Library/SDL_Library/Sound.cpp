#include "Sound.h"

Sound::Sound()
{
	path = "";
	SDL_memset(&wavSpec, 0, sizeof(wavSpec)); //https://wiki.libsdl.org/SDL_AudioSpec
	wavLength = NULL;
	wavBuffer = NULL;
	deviceid = NULL;
	paused = true;
}

Sound::Sound(const char* Path)
{
	path = Path;
	SDL_memset(&wavSpec, 0, sizeof(wavSpec)); //https://wiki.libsdl.org/SDL_AudioSpec
	wavLength = NULL;
	wavBuffer = NULL;
	deviceid = NULL;
	paused = true;
}

Sound::~Sound()
{
	SDL_CloseAudioDevice(deviceid);
	//SDL_FreeWAV(wavBuffer);
}

bool Sound::init()
{
	if (SDL_LoadWAV(path, &wavSpec, &wavBuffer, &wavLength) == NULL)
		return false;
	else
	{
		deviceid = SDL_OpenAudioDevice(NULL, 0, &wavSpec, NULL, 0);

		int success = SDL_QueueAudio(deviceid, wavBuffer, wavLength);
		SDL_PauseAudioDevice(deviceid, 1);
		paused = true;

		return true;
	}
	return false;
}

void Sound::toggle()
{
	if (paused)
	{
		cout << "play" << endl;
		SDL_PauseAudioDevice(deviceid, 0);
	}
	else if (!paused)
	{
		cout << "pause" << endl;
		SDL_PauseAudioDevice(deviceid, 1);
	}

	paused = !paused;
}

void Sound::play()
{
	SDL_FreeWAV(wavBuffer);
	SDL_PauseAudioDevice(deviceid, 0);
	paused = true;
}
