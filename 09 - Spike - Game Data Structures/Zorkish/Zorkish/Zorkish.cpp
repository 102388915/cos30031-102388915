#include <iostream>

#include "StateManager.h"

using namespace std;

int main()
{
    StateManager manager;
    string input;

    while (manager.isRunning)
    {
        manager.render();

        cin >> input;

        manager.update(input);
    }
}