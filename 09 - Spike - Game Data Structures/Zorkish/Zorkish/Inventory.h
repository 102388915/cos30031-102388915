#pragma once

#include <vector>
#include "Items.h"

using namespace std;

class Inventory
{
private:
	vector<Item*> items;
public:
	Inventory();
	void print();
	bool empty();
	void deposit(Item* item);
	Item* withdraw(string ID);
};