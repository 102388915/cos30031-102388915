#include "Inventory.h"

Inventory::Inventory() 
{
	items = vector<Item*>();
}

void Inventory::print()
{
	for (Item* item : items)
		cout << item->getName() << endl;
}

bool Inventory::empty()
{
	return items.empty();
}

void Inventory::deposit(Item* item)
{
	for (size_t i = 0; i < items.size(); i++)
	{
		if (items[i]->getID() == item->getID())
		{
			int quantity = items[i]->getQuantity() + item->getQuantity();
			items[i]->setQuantity(quantity);
			delete item;
			break;
		}
		else if (i == items.size() - 1)
			items.push_back(item);	
	}
	if (items.size() == 0)
		items.push_back(item);
}

Item* Inventory::withdraw(string ID)
{
	Item* result = nullptr;

	// find item
	for (size_t i = 0; i < items.size(); i++)
	{
		if (items[i]->getID() == ID)
		{
			result = items[i];
			vector<Item*>::iterator it;
			it = items.begin();
			for (size_t j = 0; j < i; j++)
				it++;
			items.erase(it);
			break;
		}
	}

	return result;
}