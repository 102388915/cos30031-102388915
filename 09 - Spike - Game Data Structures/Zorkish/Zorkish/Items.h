#pragma once

#include <string>
#include <iostream>

using namespace std;

class Item
{
private:
	string name;
	string description;
	int quantity;

public:
	virtual string getID() = 0;
	virtual string getName() = 0;
	virtual string getDescription() = 0;
	virtual int getQuantity() = 0;
	virtual void setQuantity(int newQuantity) = 0;

	virtual void lookAt() = 0;
	virtual void lookIn() = 0;
	virtual void open() = 0;
	virtual void close() = 0;
};

class Bag : public Item
{
private:
	string name;
	string description;
	int quantity;

	bool opened;
	bool looted;

public:
	Bag();
	string getID();
	string getName();
	string getDescription();
	int getQuantity();
	void setQuantity(int newQuantity);

	void lookAt();
	void lookIn();
	void open();
	void close();
};

class Coin : public Item
{
private:
	string name;
	string description;
	int quantity;

public:
	Coin();
	string getID();
	string getName();
	string getDescription();
	int getQuantity();
	void setQuantity(int newQuantity);

	void lookAt();
	void lookIn();
	void open();
	void close();
};