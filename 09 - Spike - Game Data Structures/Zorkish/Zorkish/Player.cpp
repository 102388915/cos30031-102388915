#include "Player.h"

Player::Player()
{
	inventory = Inventory();
	score = 0;
}

void Player::printInventory()
{
	if (inventory.empty())
		cout << "You have nothing!" << endl;
	else
	{
		cout << "You have:" << endl;
		inventory.print();
	}
}

void Player::deposit(Item* item)
{
	inventory.deposit(item);
}

Item* Player::withdraw(string name)
{
	return inventory.withdraw(name);
}