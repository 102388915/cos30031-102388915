#pragma once

#include <iostream>
#include "Player.h"

using namespace std;

enum class StateType
{
	NONE,
	MAIN_MENU,
	ABOUT,
	HELP,
	SELECTADVENTURE,
	GAMEPLAY,
	NEWHIGHSCORE,
	VIEWHALLOFFAME,
	QUIT
};

class State
{
public:
	virtual StateType update(string input) = 0;
	virtual void render() = 0;
};

class MainMenuState : public State
{
public:
	MainMenuState();
	StateType update(string input);
	void render();
};

class AboutState : public State
{
public:
	AboutState();
	StateType update(string input);
	void render();
};

class HelpState : public State
{
public:
	HelpState();
	StateType update(string input);
	void render();
};

class SelectAdventureState : public State
{
public:
	SelectAdventureState();
	StateType update(string input);
	void render();
};

class GameplayState : public State
{
private:
	Inventory worldInventory;
	Player player;
	bool started;
public:
	GameplayState();
	StateType update(string input);
	void render();
};

class NewHighScoreState : public State
{
public:
	NewHighScoreState();
	StateType update(string input);
	void render();
};

class ViewHallOfFameState : public State
{
public:
	ViewHallOfFameState();
	StateType update(string input);
	void render();
};