#include "Items.h"

Coin::Coin()
{
	name = "gold coin";
	description = "a collection of gold coins";
	quantity = 1;
}

string Coin::getID()
{
	//same as get name without quantity specifier
	return name;
}

string Coin::getName()
{
	if (quantity > 1)
		return (to_string(quantity) + " " + name + "s");
	return name;
}

string Coin::getDescription()
{
	return description;
}

int Coin::getQuantity()
{
	return quantity;
}

void Coin::setQuantity(int newQuantity)
{
	quantity = newQuantity;
}

void Coin::lookAt()
{
	cout << description << endl;
}

void Coin::lookIn()
{
	// fails unless it has its own inventory
	cout << "you cannot look in the " << name << endl;
}

void Coin::open()
{
	// fails unless it can open
	cout << "you cannot open the " << name << endl;
}

void Coin::close()
{
	// fails unless it can open
	cout << "you cannot close the " << name << endl;
}