#include "Items.h"

Bag::Bag()
{
	name = "bag";
	description = "The bag is small and tied closed with a piece of string.\nYou could probably manage to undo the knot if you tried.";
	quantity = 1;
	opened = false;
	looted = false;
}

string Bag::getID()
{
	//same as get name without quantity specifier
	return name;
}

string Bag::getName()
{
	if (quantity > 1)
		return (quantity + " " + name + "s");
	return name;
}

string Bag::getDescription()
{
	return description;
}

int Bag::getQuantity()
{
	return quantity;
}

void Bag::setQuantity(int newQuantity)
{
	quantity = newQuantity;
}

void Bag::lookAt()
{
	cout << description << endl;
	if (!looted)
		cout << "There is something in the bag." << endl;
}

void Bag::lookIn()
{
	if (opened)
	{
		if (!looted)
		{
			cout << "There are ";
			// TODO: logic for retrieving objects
			cout << getName();
			cout << " in the bag." << endl;

			cout << "Score: +30" << endl;
			looted = true;
		}
		else
			cout << "You already looted this bag" << endl;
	}
	else
		cout << "The bag is closed. You can only guess what is inside." << endl;
}

void Bag::open()
{
	if (opened)
		cout << "The bag is already opened" << endl;
	else
	{
		cout << "Your skills are poor, but you do manage to undo the knot of the bag." << endl;
		opened = true;
	}		
}

void Bag::close()
{
	if (opened)
	{
		cout << "Your skills are poor, but you do manage to redo the knot of the bag." << endl;
		opened = false;
	}
	else
		cout << "The bag is already closed" << endl;
}