#include "states.h"

SelectAdventureState::SelectAdventureState()
{
}

StateType SelectAdventureState::update(string input)
{
	// ADD DIFFERENT ADVENTURE SUPPORT
	return StateType::GAMEPLAY;
}

void SelectAdventureState::render()
{
	cout << "Choose your adventure:" << endl;
	cout << "\t1. Mountain World" << endl;
	cout << "\t2. Water World" << endl;
	cout << "\t3. Box World" << endl;
	cout << "Select 1 - 3: > " << endl;
}