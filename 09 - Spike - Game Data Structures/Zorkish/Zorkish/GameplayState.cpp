#include "states.h"

GameplayState::GameplayState()
{
	player = Player();
	worldInventory = Inventory();
	bool started = false;
}

StateType GameplayState::update(string input)
{
	if (input == "inventory")
	{
		player.printInventory();
	}
	else if (input == "takegoldcoin")
	{
		//initialise the gold coin
		Coin* coin = new Coin;
		worldInventory.deposit(coin);

		//the command system triggers a search in the worldInventory to find the gold coin
		Item* foundItem;
		foundItem = worldInventory.withdraw("gold coin");

		//if something is found, it adds it to player inventory
		if (foundItem != nullptr)
		{
			cout << "you picked up a " << foundItem->getName() << endl;
			player.deposit(foundItem);
		}

		//FIGURE OUT HOW TO PREVENT SCOPING PROBLEM
	}
	else if (input == "dropgoldcoins")
	{
		Item* foundItem;
		foundItem = player.withdraw("gold coin");

		//if something is found, it adds it to world inventory
		if (foundItem != nullptr)
		{
			cout << "you dropped " << foundItem->getName() << endl;
			worldInventory.deposit(foundItem);
		}
	}
	else if (input == "quit")
	{
		cout << "Your adventure has ended without fame or fortune." << endl;
		return StateType::MAIN_MENU;
	}
	else if (input == "hiscore")
	{
		cout << "You have entered the magic word and will now see the �New High Score� screen." << endl;
		return StateType::NEWHIGHSCORE;
	}
	return StateType::NONE;
}

void GameplayState::render()
{
	if (!started)
	{
		cout << "Welcome to Zorkish: Void World" << endl;
		cout << "This world is simple and pointless. Used it to test Zorkish phase 1 spec." << endl;
		started = true;
	}
	cout << ":>";
}