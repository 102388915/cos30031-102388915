#pragma once

#include "Inventory.h"

using namespace std;

class Player
{
private:
	Inventory inventory;
	int score;
public:
	Player();

	//inventory functions
	void printInventory();
	void deposit(Item* item);
	Item* withdraw(string ID);
};