#include <iostream>
#include <stdlib.h> 
#include <vector>

using namespace std;

void PrintValues(string aVal, string bVal)
{
	//takes 2 strings and prints them.
	cout << aVal << endl;
	cout << bVal << endl << endl;
}

int AddOne(int input)
{
	//adds 1 to an int and returns it
	return input + 1;
}

void PrintOdds()
{
	//loops and prints all odd numbers
	for (int i = 0; i < 20; i++)
	{
		if (i % 2 != 0)
			cout << i << endl;
	}
	cout << endl;
}

void PrintInts()
{
	//generates 5 random ints and prints them
	int intarray[5];
	for (int i = 0; i < 5; i++)
	{
		intarray[i] = rand();
		cout << intarray[i] << endl;
	}
	cout << endl;
}

class Dog
{
	//Demonstration of classes. dog may be named and patted.
private:
	int age;
public:
	string name;
	Dog() 
	{
		name = "Doggy";
		age = 10;
	}
	void Pat()
	{
		cout << name << " expresses gratitude." << endl;
	}
};

int main()
{
	//Demo Feature 1
	PrintValues("a", "b");


	//Demo Feature 2
	cout << 55 << endl;
	cout << AddOne(55) << endl << endl;


	//Demo Feature 3
	int fiftyfive = 55;
	int* fiftyfiveptr = &fiftyfive;

	cout << fiftyfive << endl;
	cout << fiftyfiveptr << endl << endl;


	//Demo Feature 4
	PrintOdds();


	//Demo Feature 5
	PrintInts();


	//Demo Feature 6
	string text = "this has spaces in it";
	string delimiter = " ";
	int pos;
	vector<string> texts;
	while ((pos = text.find(delimiter)) != string::npos) 
	{
		texts.push_back(text.substr(0, pos));
		//cout << token << endl;
		//texts.push_back(token);
		text.erase(0, pos + delimiter.length());
	}

	texts.push_back(text);
	for (auto i = texts.begin(); i != texts.end(); ++i)
		cout << *i << " ";
	cout << endl << endl;


	//Demo Feature 7
	Dog dog;
	dog.name = "Bernie";
	dog.Pat();

	return 0;
}