#include "Graph.h"

Graph::Graph()
{
    playerX = NULL;
    playerY = NULL;
}

Graph::Graph(int playerx, int playery, map<int, map<int, Room>> m)
{
	playerX = playerx;
	playerY = playery;
    M = m;
}

void Graph::north()
{
    if (M[playerX][playerY - 1].Name != "")
    {
        playerY -= 1;
        cout << "You move north" << endl;
    }
    else
        cout << "You can't move north" << endl;
}

void Graph::south()
{
    if (M[playerX][playerY + 1].Name != "")
    {
        playerY += 1;
        cout << "You move south" << endl;
    }
    else
        cout << "You can't move south" << endl;
}

void Graph::east()
{
    if (M[playerX + 1][playerY].Name != "")
    {
        playerX += 1;
        cout << "You move east" << endl;
    }
    else
        cout << "You can't move east" << endl;
}

void Graph::west()
{
    if (M[playerX - 1][playerY].Name != "")
    {
        playerX -= 1;
        cout << "You move west" << endl;
    }
    else
        cout << "You can't move west" << endl;
}

void Graph::look()
{
    cout << "You are in the " << M[playerX][playerY].Name << endl;
    cout << M[playerX][playerY].Description << endl;
}

string Graph::directions()
{
    string result = "";
    if (M[playerX][playerY - 1].Name != "")
        result += "North, ";
    if (M[playerX][playerY + 1].Name != "")
        result += "South, ";
    if (M[playerX + 1][playerY].Name != "")
        result += "East, ";
    if (M[playerX - 1][playerY].Name != "")
        result += "West, ";

    if (!result.empty())
    {
        result.pop_back();
        result.pop_back();
    }

    return result;
}