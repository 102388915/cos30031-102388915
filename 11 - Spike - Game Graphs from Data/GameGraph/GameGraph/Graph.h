#pragma once
#include <map>
#include <iostream>

#include "Room.h"

using namespace std;

class Graph
{
private:
	int playerX;
	int playerY;
	map<int, map<int, Room>> M;
public:
	Graph();
	Graph(int playerx, int playery, map<int, map<int, Room>> m);
	void north();
	void south();
	void east();
	void west();
	void look();
	string directions();
};