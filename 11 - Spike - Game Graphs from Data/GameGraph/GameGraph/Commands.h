#pragma once


class Command
{
public:
	virtual void execute() = 0;
};

class GoCommand : Command
{
public:
	override void execute();
};

class QuitCommand : Command
{
public:
	override void execute();
};