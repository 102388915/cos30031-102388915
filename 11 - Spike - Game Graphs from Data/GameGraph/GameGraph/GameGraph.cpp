// GameGraph.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <iterator>
#include <map>
#include <fstream>
#include <vector>

#include "Room.h"
#include "Player.h"
#include "Graph.h"

//Global Variables
//Map of Maps of Rooms
Graph graph;


void render()
{
    cout << "You can move: " << graph.directions() << endl;
    cout << "What would you like to do?" << endl;
}

bool update(string input)
{
    if (input == "go north")
        graph.north();
    else if (input == "go south")
        graph.south();
    else if (input == "go east")
        graph.east();
    else if (input == "go west")
        graph.west();

    else if (input == "look")
        graph.look();

    else if (input == "quit")
            return true;

    else
        cout << "input error" << endl;

    return false;
}


vector<string> split(string s, string delimiter)
{
    vector<string> result;
    size_t pos = 0;
    string token;
    while ((pos = s.find(delimiter)) != string::npos) {
        token = s.substr(0, pos);
        result.push_back(token);
        //cout << token << endl;
        s.erase(0, pos + delimiter.length());
    }
    //cout << s << endl;
    result.push_back(s);
    return result;
}


int main()
{
    std::cout << "THIS IS A TEST GRID MAP" << endl;

    //load map file
    ifstream file("world.txt");
    if (file.is_open())
    {
        string line;
        map<int, map<int, Room>> m;

        while (getline(file, line))
        {
            vector<string> instructions = split(line, ";");
            int x = stoi(instructions[0]);
            int y = stoi(instructions[1]);
            string name = instructions[2];
            string inventory = instructions[3];

            Room room(name, inventory);

            m.insert(make_pair(x, map<int, Room>()));
            m[x].insert(make_pair(y, room));
        }
        graph = Graph(1, 1, m);
        file.close();
    }
    else
        cout << "failed to read file" << endl;

    //test map load 
    /* map<int, map<int, Room> >::iterator itr;
    map<int, Room>::iterator ptr;
    for (itr = m.begin(); itr != m.end(); itr++) {
        for (ptr = itr->second.begin(); ptr != itr->second.end(); ptr++) {
            cout << "First key is " << itr->first
                << " And second key is " << ptr->first
                << " And value is " << ptr->second.Name << endl;
        }
    } */

    //game loop
    bool terminate = false;
    string input;

    while (terminate == false)
    {
        render();

        getline(cin, input);

        terminate = update(input);

        cout << endl;
    }

    cout << "Goodbye" << endl;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
