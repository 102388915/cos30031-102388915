#pragma once
#include <string>

using namespace std;

class Room
{
public:
	Room();
	Room(string name, string description);
	string Name;
	string Description;
};