#include <SDL.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <map>
#include <iostream>

using namespace std;

const int SCREEN_WIDTH = 600;
const int SCREEN_HEIGHT = 800;

bool running = true;

bool init();
void update();
void randomise();
void close();

SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;

bool init()
{
    bool result = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
        result = false;
    }
    else
    {
        window = SDL_CreateWindow("SDL Test", 
                    SDL_WINDOWPOS_CENTERED, 
                    SDL_WINDOWPOS_CENTERED,
                    SCREEN_WIDTH, SCREEN_HEIGHT, 
                    SDL_WINDOW_SHOWN);

        if (window == NULL)
        {
            printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
            result = false;
        }
        else
        {
            //Get window surface
            renderer = SDL_CreateRenderer(window, -1, 0);
            //initialise green (0, 255, 0)
            SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
            SDL_RenderClear(renderer);
            SDL_RenderPresent(renderer);
        }
    }

    return result;
}

void update()
{
    //check input and trigger randmomise or quit 
    // Source: https://www.youtube.com/watch?v=Gjhvz4banWA
    SDL_Event event;
    const Uint8* keystates = SDL_GetKeyboardState(NULL);
    while (SDL_PollEvent(&event))
    {
        if (event.type == SDL_QUIT)
            running = false;
        if (keystates[SDL_SCANCODE_R]) 
            randomise();
    }
}

void randomise()
{
    srand(time(NULL));
    int red = rand() % 256;
    int green = rand() % 256;
    int blue = rand() % 256;

    cout << "randomising to: "<< red << "," << green << "," << blue << endl;

    //initialise green (0, 255, 0)
    SDL_SetRenderDrawColor(renderer, red, green, blue, 255);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);
}

void close()
{
    //Destroy window
    SDL_DestroyWindow(window);
    window = NULL;

    //Quit SDL subsystems
    SDL_Quit();
}

int main(int argc, char* args[])
{
    //Start up SDL and create window
    if (!init())
    {
        printf("Failed to initialize!\n");
        return 1;
    }

    while (running)
    {
        update();
    }


    //Give us time to see the window.
    //SDL_Delay(5000);

    //Free resources and close SDL
    close();

	return 0;
}