#include <iostream>
#include <fstream>
#include <string>

#include "CompoundType.h"
#include "json.hpp"

using namespace std;
using json = nlohmann::json;

int main()
{
    ///////////////
    // Section 1 //
    ///////////////
    /*
    cout << "Part A: 1-4" << endl;
    CompoundType object1 = CompoundType();
    object1.character = 'a';
    object1.integer = 1;
    object1.floatation = 1.1;

    object1.print();
    */

    ///////////////
    // Section 2 //
    ///////////////
    /*
    cout << endl << "Part A: 5-9" << endl;
    ofstream file;
    file.open("test1.bin");
    file << object1.character << "\n";
    file << object1.integer << "\n";
    file << object1.floatation;
    file.close();
    */

    ///////////////
    // Section 3 //
    ///////////////
    /*
    cout << endl << "Part A: 10-11" << endl;
    
    CompoundType object2 = CompoundType();
    ifstream file;

    file.open("test1.bin");
    if (file.is_open())
    {
        string line;

        getline(file, line);
        object2.character = line[0];
        getline(file, line);
        object2.integer = stoi(line);
        getline(file, line);
        object2.floatation = stof(line);

        file.close();
    }

    object2.print();
    */

    ///////////////
    // Section 4 //
    ///////////////
    /*
    cout << endl << "Part B: 1-4" << endl;

    ifstream file("test2.txt");
    
    if (file.is_open())
    {
        string line;

        while (getline(file, line))
        {
            cout << line << endl;
        }

        file.close();
    }
    else
        cout << "failed to read file" << endl;
    */

    ///////////////
    // Section 5 //
    ///////////////
    /*
    cout << endl << "Part B: 5-6" << endl;

    ifstream file("test2.txt");

    if (file.is_open())
    {
        string line;

        while (getline(file, line))
        {
            if (!(line[0] == '#' || line.empty())) //if line isnt empty or commented
            {
                // split string by ' ' delimiter
                string str = "";
                for (int i = 0; i < line.size(); i++) {
                    if (line[i] != ' ') {
                        str += line[i];
                    }
                    else {
                        cout << str << endl;
                        str = "";
                    }
                }
                cout << str << endl;
            }
        }

        file.close();
    }
    else
        cout << "failed to read file" << endl;
    */

    ///////////////
    // Section 6 //
    ///////////////
    cout << endl << "Part C" << endl;

    json j;

    ifstream file("test3.json");
    if (file.is_open())
    {
        string line;
        getline(file, line);
        j = json::parse(line);
    }

    for (auto& el : j.items())
    {
        cout << el.key() << ": " << el.value() << endl;
    }

    return 0;
}