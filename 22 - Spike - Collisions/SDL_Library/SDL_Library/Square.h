#pragma once

#include <SDL.h>
#include <vector>

#include "GlobalVariables.h"

class Square
{
private:
	SDL_Rect rect;

	int xVel;
	int yVel;

public:
	Square(int x, int y, int w, int h, int xvel, int yvel);

	void Update(std::vector<Square*> squares);
	void Render(SDL_Renderer* renderer);

	bool Collided(std::vector<Square*>);
};