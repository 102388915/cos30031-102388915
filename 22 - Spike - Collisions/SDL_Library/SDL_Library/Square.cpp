#include "Square.h"

Square::Square(int x, int y, int w, int h, int xvel, int yvel)
{
	rect.x = x;
	rect.y = y;

	rect.w = w;
	rect.h = h;

	xVel = xvel;
	yVel = yvel;
}

void Square::Update(std::vector<Square*> squares)
{
	if (Collided(squares))
	{
		//if collision, flip course
		xVel = xVel * -1;
		yVel = yVel * -1;
	}
	
	//move
	rect.x += xVel;
	rect.y += yVel;
}

void Square::Render(SDL_Renderer* renderer)
{
	SDL_RenderFillRect(renderer, &rect);
}

bool Square::Collided(std::vector<Square*> squares)
{
	//check borders
	if (rect.x <= 0 || (rect.x + rect.w >= SCREEN_WIDTH))
		return true;
	if (rect.y <= 0 || (rect.y + rect.h >= SCREEN_HEIGHT))
		return true;

	const SDL_Rect* temp1 = new SDL_Rect(rect);

	//check other squares
	for (Square* s : squares)
	{
		if (s != this)
		{
			const SDL_Rect* temp2 = new SDL_Rect(s->rect);
			if (SDL_HasIntersection(temp1, temp2))
				return true;
		}
	}

	return false;
}
