#pragma once

#include <vector>
#include <string>
#include <SDL.h>

#include "GlobalVariables.h"

class Circle
{
private:
	int centreX;
	int centreY;

	int radius;

	int xVel;
	int yVel;
public:
	Circle(int x, int y, int r, int xvel, int yvel);

	void Update(std::vector<Circle*> circles);
	void Render(SDL_Renderer* renderer);

	bool Collided(std::vector<Circle*> circles);
};