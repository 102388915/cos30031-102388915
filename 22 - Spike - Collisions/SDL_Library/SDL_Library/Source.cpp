#include <SDL.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <map>
#include <iostream>

#include "Square.h"
#include "Circle.h"
#include "GlobalVariables.h"

using namespace std;

bool running = true;

SDL_Rect randomrect(int width, int height);
bool init();
void update();
void close();

bool toggleSquares = true;
bool toggleCircles = true;

int osize = 50;

//square details
Square square1(275, 1, 50, 50, 0, 1);	//moving up-down
Square square2(1, 200, 50, 50, 3, 0);	//moving right-left
Square square3(275, 300, 50, 50, 0, 0);	//stationary centre
vector<Square*> squares = {&square1, &square2, &square3};

//circle details
Circle circle1(300, 774, 25, 0, -1);	//moving up-down
Circle circle2(26, 600, 25, 3, 0);		//moving left
Circle circle3(300, 475, 25, 0, 0);		//stationary centre
vector<Circle*> circles = {&circle1, &circle2, &circle3};

//rendering tools
SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;
SDL_Surface* screenSurface = NULL;


bool init()
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		return false;
	}
	else
	{
		//create window
		window = SDL_CreateWindow("SDL Test", 
					SDL_WINDOWPOS_CENTERED, 
					SDL_WINDOWPOS_CENTERED,
					SCREEN_WIDTH, SCREEN_HEIGHT, 
					SDL_WINDOW_SHOWN);

		if (window == NULL)
		{
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
			return false;
		}
		else
		{
			//create renderer
			renderer = SDL_CreateRenderer(window, -1, 0);
			screenSurface = SDL_GetWindowSurface(window);

			SDL_SetRenderDrawColor(renderer, 120, 120, 120, 255);	//white bg

			SDL_RenderClear(renderer);
			SDL_RenderPresent(renderer);
		}
	}

	return true;
}

void render()
{
	SDL_RenderClear(renderer);
		
	//render things
	if (toggleSquares)
	{
		SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255);
		for (Square* s : squares)
			s->Render(renderer);
	}
	if (toggleCircles)
	{
		SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
		for (Circle* c : circles)
			c->Render(renderer);
	}

	SDL_SetRenderDrawColor(renderer, 120, 120, 120, 255);
	SDL_RenderPresent(renderer);
}

void update()
{
	//check input and trigger randmomise or quit 
	// Source: https://www.youtube.com/watch?v=Gjhvz4banWA
	SDL_Event event;
	const Uint8* keystates = SDL_GetKeyboardState(NULL);
	while (SDL_PollEvent(&event))
	{
		if (event.type == SDL_QUIT)
			running = false;
		if (keystates[SDL_SCANCODE_1] || keystates[SDL_SCANCODE_KP_1])
		{
			cout << "Toggle squares" << endl;
			toggleSquares = !toggleSquares;
			SDL_Delay(200);
		}
		if (keystates[SDL_SCANCODE_2] || keystates[SDL_SCANCODE_KP_2])
		{
			cout << "Toggle circles" << endl;
			toggleCircles = !toggleCircles;
			SDL_Delay(200);
		}
	}

	if (toggleSquares)
	{
		for (Square* s : squares)
			s->Update(squares);
	}
	if (toggleCircles)
	{
		for (Circle* c : circles)
			c->Update(circles);
	}
}

void close()
{
	//destroy texturesif (toggleCircles)
	{
		for (Circle* c : circles)
			c->Render(renderer);
	}
	SDL_DestroyRenderer(renderer);

	//Destroy window
	SDL_DestroyWindow(window);
	window = NULL;

	//Quit SDL subsystems
	SDL_Quit();
}

int main(int argc, char* args[])
{
	//Start up SDL and create window
	if (!init())
	{
		printf("Failed to initialize!\n");
		return 1;
	}

	while (running)
	{
		render();
		update();
		SDL_Delay(10);
	}

	//Free resources and close SDL
	close();

	return 0;
}
