#include "Circle.h"

Circle::Circle(int x, int y, int r, int xvel, int yvel)
{
	centreX = x;
	centreY = y;
	radius = r;
	xVel = xvel;
	yVel = yvel;
}

void Circle::Update(std::vector<Circle*> circles)
{
	if (Collided(circles))
	{
		//if collision, flip course
		xVel = -xVel;
		yVel = -yVel;
	}
	
	//move
	centreX += xVel;
	centreY += yVel;
}

void Circle::Render(SDL_Renderer* renderer)
{
	// circle rendering function reference:
	// https://stackoverflow.com/questions/38334081/howto-draw-circles-arcs-and-vector-graphics-in-sdl

	const int32_t diameter = (radius * 2);

	int32_t x = (radius - 1);
	int32_t y = 0;
	int32_t tx = 1;
	int32_t ty = 1;
	int32_t error = (tx - diameter);

	while (x >= y)
	{
		//  Each of the following renders an octant of the circle
		SDL_RenderDrawPoint(renderer, centreX + x, centreY - y);
		SDL_RenderDrawPoint(renderer, centreX + x, centreY + y);
		SDL_RenderDrawPoint(renderer, centreX - x, centreY - y);
		SDL_RenderDrawPoint(renderer, centreX - x, centreY + y);
		SDL_RenderDrawPoint(renderer, centreX + y, centreY - x);
		SDL_RenderDrawPoint(renderer, centreX + y, centreY + x);
		SDL_RenderDrawPoint(renderer, centreX - y, centreY - x);
		SDL_RenderDrawPoint(renderer, centreX - y, centreY + x);

		if (error <= 0)
		{
			++y;
			error += ty;
			ty += 2;
		}

		if (error > 0)
		{
			--x;
			tx += 2;
			error += (tx - diameter);
		}
	}
}

bool Circle::Collided(std::vector<Circle*> circles)
{
	//check borders
	if (centreX - radius <= 0 || (centreX + radius >= SCREEN_WIDTH))
		return true;
	if (centreY - radius <= 0 || (centreY + radius >= SCREEN_HEIGHT))
		return true;

	for (Circle* c : circles)
	{
		if (this != c)
		{
			//Calculate total radius squared
			int totalRadiusSquared = this->radius + c->radius;
			totalRadiusSquared = totalRadiusSquared * totalRadiusSquared;

			//calculate distance squared
			int deltaX = c->centreX - this->centreX;
			int deltaY = c->centreY - this->centreY;

			//If the distance between the centers of the circles is less than the sum of their radii
			if (deltaX * deltaX + deltaY * deltaY < (totalRadiusSquared))
			{
				//The circles have collided
				return true;
			}
		}
	}
	
	return false;
}
