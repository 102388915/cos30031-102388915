#ifndef STATESINCLUDED
#define STATESINCLUDED
#include "States.h"
#endif

MainMenuState::MainMenuState()
{
}

StateType MainMenuState::update(string input)
{
	if (input == "1")
		return StateType::SELECTADVENTURE;
	else if (input == "2")
		return StateType::VIEWHALLOFFAME;
	else if (input == "3")
		return StateType::HELP;
	else if (input == "4")
		return StateType::ABOUT;
	else if (input == "5")
		return StateType::QUIT;
	return StateType::NONE;
}

void MainMenuState::render()
{
	cout << "Welcome to Zorkish Adventures" << endl;

	cout << "\t1. Select Adventure and Play" << endl;
	cout << "\t2. Hall Of Fame" << endl;
	cout << "\t3. Help" << endl;
	cout << "\t4. About" << endl;
	cout << "\t5. Quit" << endl;

	cout << "Select 1-5:> ";
}