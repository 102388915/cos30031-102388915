#pragma once

#ifndef MAPINCLUDED
#include <map>
#define MAPINCLUDED
#endif

#ifndef STRINGINCLUDED
#include <string>
#define STRINGINCLUDED
#endif

#include "Commands.h"

using namespace std;

class CommandManager
{
private:
	map <CommandType, Command*> commands;

	vector<string> SplitString(string s, string delimiter);
public:
	CommandManager();
	void execute(string arg, Graph* graph);
};