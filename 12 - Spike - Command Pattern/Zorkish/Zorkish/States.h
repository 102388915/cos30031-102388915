#pragma once

#include "CommandManager.h"
#include "Graph.h"

//using namespace std;

enum class StateType
{
	NONE,
	MAIN_MENU,
	ABOUT,
	HELP,
	SELECTADVENTURE,
	GAMEPLAY,
	NEWHIGHSCORE,
	VIEWHALLOFFAME,
	QUIT
};

class State
{
public:
	virtual StateType update(string input) = 0;
	virtual void render() = 0;
};

class MainMenuState : public State
{
public:
	MainMenuState();
	StateType update(string input);
	void render();
};

class AboutState : public State
{
public:
	AboutState();
	StateType update(string input);
	void render();
};

class HelpState : public State
{
public:
	HelpState();
	StateType update(string input);
	void render();
};

class SelectAdventureState : public State
{
public:
	SelectAdventureState();
	StateType update(string input);
	void render();

	string WorldSelect(string input);
};

class GameplayState : public State
{
private:
	bool started;

public:
	CommandManager manager;
	Graph graph;
	Player player;

	GameplayState();
	StateType update(string input);
	void render();

	bool WorldGen(string world);
};

class NewHighScoreState : public State
{
public:
	NewHighScoreState();
	StateType update(string input);
	void render();
};

class ViewHallOfFameState : public State
{
public:
	ViewHallOfFameState();
	StateType update(string input);
	void render();
};