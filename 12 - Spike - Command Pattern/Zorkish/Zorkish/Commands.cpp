#include "Commands.h"

void GoCommand::execute(string arg, Graph* graph)
{
	if (arg.find("north") != string::npos)
		graph->north();
	else if (arg.find("south") != string::npos)
		graph->south();
	else if (arg.find("east") != string::npos)
		graph->east();
	else if (arg.find("west") != string::npos)
		graph->west();
	else
		cout << "error in go command input" << endl;
}

void HelpCommand::execute(string arg, Graph* graph)
{
	// handled in command manager
}

void InventoryCommand::execute(string arg, Graph* graph)
{
	if (arg.find("inventory") != string::npos)
		graph->player->printInventory();
}

void LookCommand::execute(string arg, Graph* graph)
{
	graph->look();
}

void LookAtCommand::execute(string arg, Graph* graph)
{
	// isolate what we're looking at
	size_t pos = arg.find(alias + " ");
	if (pos != std::string::npos)
		arg.erase(pos, alias.length()+1);

	// remove number and plural
	if (!isalpha(arg[0]))
		arg.erase(0, 2);

	// remove plural
	if (arg[arg.size()-1] == 's')
		arg.pop_back();
		
	cout << "trying to find: " << arg << endl;

	if (graph->player->finditem(arg) != nullptr)
	{
		cout << graph->player->finditem(arg)->getDescription() << endl;
	}
	else if (graph->finditem(arg) != nullptr)
	{
		cout << graph->finditem(arg)->getDescription() << endl;
	}
	else
		cout << "error could not find" << endl;
}

void DebugTreeCommand::execute(string arg, Graph* graph)
{
	graph->debug();

	cout << "player coords: " << graph->player->x << "," << graph->player->y << endl;
	graph->player->printInventory();
}
