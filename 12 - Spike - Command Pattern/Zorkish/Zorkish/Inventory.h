#pragma once

#ifndef VECTORINCLUDED
#include <vector>
#define VECTORINCLUDED
#endif

#include "Items.h"

//using namespace std;

class Inventory
{
private:
	vector<Item*> items;
public:
	Inventory();
	~Inventory();
	void print();
	bool empty();
	void deposit(Item* item);
	Item* withdraw(string ID);
	Item* finditem(string ID);
};