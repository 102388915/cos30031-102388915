#pragma once

#include "Inventory.h"
//#include "SplitString.cpp"

//using namespace std;

class Room
{
private:
	vector<string> SplitString(string s, string delimiter);
public:
	string Name;
	Inventory Inventory;

	Room();
	Room(string name, string inventorystring);

	//inventory functions
	void PrintInventory();
	void deposit(Item* item);
	Item* withdraw(string ID);
	Item* finditem(string ID);
};