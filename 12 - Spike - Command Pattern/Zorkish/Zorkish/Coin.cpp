#include "Items.h"

Coin::Coin()
{
	name = "Gold coin";
	description = "A collection of gold coins";
	quantity = 1;
}

void Coin::lookAt()
{
	cout << getDescription() << endl;
}

void Coin::lookIn()
{
	// fails unless it has its own inventory
	cout << "you cannot look in the " << name << endl;
}

void Coin::open()
{
	// fails unless it can open
	cout << "you cannot open the " << name << endl;
}

void Coin::close()
{
	// fails unless it can open
	cout << "you cannot close the " << name << endl;
}