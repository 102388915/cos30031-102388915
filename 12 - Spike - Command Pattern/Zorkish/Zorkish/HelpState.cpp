#ifndef STATESINCLUDED
#define STATESINCLUDED
#include "States.h"
#endif

HelpState::HelpState()
{
}

StateType HelpState::update(string input)
{
	return StateType::MAIN_MENU;
}

void HelpState::render()
{
	cout << "The following commands are supported:" << endl;
	cout << "\tquit," << endl;
	cout << "\thiscore (for testing)," << endl;
	cout << "Press Enter to return to the Main Menu" << endl;
}