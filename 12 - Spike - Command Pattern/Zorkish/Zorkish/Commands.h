#pragma once

#include "Graph.h"

//using namespace std;

enum class CommandType
{
	NONE,
	GO,
	HELP,
	INVENTORY,
	LOOK,
	LOOKAT,
	ALIAS,
	DEBUGTREE,
	QUIT
};

class Command
{
public:
	string alias;
	virtual void execute(string arg, Graph* graph) = 0;
};

class GoCommand : public Command
{
public:
	void execute(string arg, Graph* graph);
};

class HelpCommand : public Command
{
public:
	void execute(string arg, Graph* graph);
};

class InventoryCommand : public Command
{
public:
	void execute(string arg, Graph* graph);
};

class LookCommand : public Command
{
public:
	void execute(string arg, Graph* graph);
};

class LookAtCommand : public Command
{
public:
	void execute(string arg, Graph* graph);
};

class DebugTreeCommand : public Command
{
public:
	void execute(string arg, Graph* graph);
};