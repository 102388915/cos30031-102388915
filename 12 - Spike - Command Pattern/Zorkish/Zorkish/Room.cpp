#include "Room.h"


vector<string> Room::SplitString(string s, string delimiter)
{
	vector<string> result;
	size_t pos = 0;
	string token;
	while ((pos = s.find(delimiter)) != string::npos) {
		token = s.substr(0, pos);
		result.push_back(token);
		s.erase(0, pos + delimiter.length());
	}
	result.push_back(s);
	return result;
}

Room::Room()
{
	Name = "";
}

Room::Room(string name, string inventorystring)
{
	Name = name;
	vector<string> instructions = SplitString(inventorystring, ",");
	for (string itemstring : instructions)
	{
		if (itemstring == "Coin")
		{
			Coin* coin = new Coin();
			Inventory.deposit(coin);
		}
		else if (itemstring == "Bag")
		{
			Bag* bag = new Bag();
			Inventory.deposit(bag);
		}
		else
		{
			cout << "error finding premade object: " << itemstring << "... making default ITEM type" << endl;
			Item* item = new Item(itemstring, "", 1);
			Inventory.deposit(item);
		}
	}
}

void Room::PrintInventory()
{
	Inventory.print();
}

void Room::deposit(Item* item)
{
	Inventory.deposit(item);
}

Item* Room::withdraw(string ID)
{
	return Inventory.withdraw(ID);
}

Item* Room::finditem(string ID)
{
	return Inventory.finditem(ID);
}
