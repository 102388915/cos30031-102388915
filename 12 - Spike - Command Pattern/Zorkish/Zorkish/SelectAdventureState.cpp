#ifndef STATESINCLUDED
#define STATESINCLUDED
#include "States.h"
#endif

SelectAdventureState::SelectAdventureState()
{
}

StateType SelectAdventureState::update(string input)
{
	string worldname = "";

	if (input == "1")
		worldname = "MountainWorld.txt";

	else if (input == "2")
		worldname = "WaterWorld.txt";

	else if (input == "3")
		worldname = "BoxWorld.txt";

	if (worldname != "")
	{
		try
		{
			ifstream file(worldname);
			if (file.is_open())
			{
				file.close();
				return StateType::GAMEPLAY;
			}
			else
				cout << "Error: file did not open" << endl;
		}
		catch (exception e)
		{	cout << e.what();	}
	}

	else
		cout << "incorrect input" << endl;

	return StateType::NONE;
}

void SelectAdventureState::render()
{
	cout << "Choose your adventure:" << endl;
	cout << "\t1. Mountain World" << endl;
	cout << "\t2. Water World" << endl;
	cout << "\t3. Box World" << endl;
	cout << "Select 1 - 3: > " << endl;
}

string SelectAdventureState::WorldSelect(string input)
{
	string worldname = "";

	if (input == "1")
		worldname = "MountainWorld.txt";

	else if (input == "2")
		worldname = "WaterWorld.txt";

	else if (input == "3")
		worldname = "BoxWorld.txt";

	return worldname;
}
