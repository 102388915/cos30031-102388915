#pragma once

#ifndef STRINGINCLUDED
#include <string>
#define STRINGINCLUDED
#endif

#include <iostream>
#include <algorithm>
#include <cctype>

using namespace std;

class Item
{
public:
	string name;
	string description;
	int quantity;

	Item();
	Item(string newname, string newdescription, int newquantity);

	string getID();
	string getName();
	string getDescription();
	int getQuantity();
	void setQuantity(int newQuantity);

	void lookAt();
	void lookIn();
	void open();
	void close();
};

class Bag : public Item
{
private:
	bool opened;
	bool looted;

public:
	Bag();

	void lookAt();
	void lookIn();
	void open();
	void close();
};

class Coin : public Item
{
public:
	Coin();

	void lookAt();
	void lookIn();
	void open();
	void close();
};