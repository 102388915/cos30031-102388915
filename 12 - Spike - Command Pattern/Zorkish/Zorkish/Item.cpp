#include "Items.h"

Item::Item()
{
	name = "";
	description = "";
	quantity = 1;
}

Item::Item(string newname, string newdescription, int newquantity)
{
	name = newname;
	description = newdescription;
	quantity = newquantity;
}

string Item::getID()
{
	//same as get name without capitals or quantity specifier
	string id = name;
	
	std::transform(id.begin(), id.end(), id.begin(),
		[](unsigned char c) { return std::tolower(c); });

	return id;
}

string Item::getName()
{
	if (quantity > 1)
		return (to_string(quantity) + " " + name + "s");
	return name;
}

string Item::getDescription()
{
	if (description != "")
		return description;
	return "There is no description for this object";
}

int Item::getQuantity()
{
	return quantity;
}

void Item::setQuantity(int newQuantity)
{
	quantity = newQuantity;
}

void Item::lookAt()
{
	cout << description << endl;
}

void Item::lookIn()
{
	// fails unless it has its own inventory
	cout << "you cannot look in the " << name << endl;
}

void Item::open()
{
	// fails unless it can open
	cout << "you cannot open the " << name << endl;
}

void Item::close()
{
	// fails unless it can open
	cout << "you cannot close the " << name << endl;
}