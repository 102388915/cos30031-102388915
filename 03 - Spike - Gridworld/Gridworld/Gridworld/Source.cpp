// Source.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "GridWorld.h"

int main()
{
    GridWorld world;
    string input = "";

    cout << "Welcome to GridWorld: Quantised Excitement. Fate is waiting for You!\n" <<
        "Valid commands: N, S, E and W for direction. Q to quit the game. M to show map\n";

    while (world.terminate == false)
    {
        input = world.GetInput();

        world.Update(input);

        if (input == "m")
            world.Render();
    }
}