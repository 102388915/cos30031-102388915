#pragma once

using namespace std;

#include <iostream>
#include "GridWorld.h"

GridWorld::GridWorld()
{
    mapSize = 8;

    map.push_back("########");
    map.push_back("#G D#D #");
    map.push_back("#   #  #");
    map.push_back("### # D#");
    map.push_back("#   #  #");
    map.push_back("#  ### #");
    map.push_back("#      #");
    map.push_back("########");

    playerRow = 7;
    playerCol = 2;

    terminate = false;
}

string GridWorld::GetInput()
{
    string print = "";

    print += "You can move ";
    if (playerRow != 0 && 
        map[playerRow - 1][playerCol] != '#')
        print += "N,";

    if (playerRow != mapSize-1 && 
        map[playerRow + 1][playerCol] != '#')
        print += "S,";

    if (playerCol != mapSize-1 && 
        map[playerRow][playerCol + 1] != '#')
        print += "E,";

    if (playerRow != 0 && 
        map[playerRow][playerCol - 1] != '#')
        print += "W,";

    print = print.substr(0, print.size() - 1);
    print += ":>";

    cout << print;

    string input;
    cin >> input;
    return input;
}

void GridWorld::Update(string input)
{
    //input
    if (input == "q")
    {
        terminate = true; 
        cout << "Thanks for playing. Maybe next time.";
    }

    if (input == "n" && 
        playerRow != 0 &&
        map[playerRow - 1][playerCol] != '#')
        --playerRow;

    if (input == "s" && 
        playerRow != mapSize - 1 &&
        map[playerRow + 1][playerCol] != '#')
        ++playerRow;

    if (input == "e" && 
        playerCol != mapSize - 1 &&
        map[playerRow][playerCol + 1] != '#')
        ++playerCol;

    if (input == "w" && 
        playerRow != 0 &&
        map[playerRow][playerCol - 1] != '#')
        --playerCol;


    //is dead?
    if (map[playerRow][playerCol] == 'D')
    {
        cout << "Arrrrgh... you've fallen down a pit.\nYOU HAVE DIED!\nThanks for playing. Maybe next time." << endl;
        terminate = true;
    }


    //is win?
    if (map[playerRow][playerCol] == 'G')
    {
        cout << "Wow - you've discovered a large chest filled with GOLD coins!\nYOU WIN!\nThanks for playing. There probably won't be a next time." << endl;
        terminate = true;
    }
}

void GridWorld::Render()
{
    string oldString = map[playerRow];
    map[playerRow][playerCol] = 'P';

    for (int row = 0; row < mapSize; row++)
    {
        cout << map[row] + "\n";
    }

    map[playerRow] = oldString;
}