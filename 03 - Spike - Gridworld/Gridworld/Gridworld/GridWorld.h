#pragma once

using namespace std;

#include <string>
#include <vector>

class GridWorld
{
public:
	GridWorld();
	string GetInput();
	void Update(string input);
	void Render();

	bool terminate;

private:
	vector<string> map;
	int mapSize;
	int playerRow;
	int playerCol;
};