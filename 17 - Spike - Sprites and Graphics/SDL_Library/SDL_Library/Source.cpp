#include <SDL.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <map>
#include <iostream>

using namespace std;

const int SCREEN_WIDTH = 600;
const int SCREEN_HEIGHT = 800;

bool running = true;

SDL_Rect randomrect(int width, int height);
bool init();
void update();
void close();

bool showimagebg = true;
bool showimage1 = false;
bool showimage2 = false;
bool showimage3 = false;

SDL_Texture* imagebg;
SDL_Texture* images;

SDL_Rect image1; SDL_Rect image1loc;
SDL_Rect image2; SDL_Rect image2loc;
SDL_Rect image3; SDL_Rect image3loc;

const char* directory_imagebg = "bg_sample.bmp";
const char* directory_images = "sample.bmp";

SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;
SDL_Texture* texture = NULL;

SDL_Rect randomrect(int width, int height)
{
    SDL_Rect result;
    srand(time(0));

    result.x = rand() % SCREEN_WIDTH;
    result.y = rand() % SCREEN_HEIGHT;
    result.w = width;
    result.h = height;

    return result;
}

bool init()
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
        return false;
    }
    else
    {
        //create window
        window = SDL_CreateWindow("SDL Test", 
                    SDL_WINDOWPOS_CENTERED, 
                    SDL_WINDOWPOS_CENTERED,
                    SCREEN_WIDTH, SCREEN_HEIGHT, 
                    SDL_WINDOW_SHOWN);

        if (window == NULL)
        {
            printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
            return false;
        }
        else
        {
            //create renderer
            renderer = SDL_CreateRenderer(window, -1, 0);


            //load textures
            SDL_Surface* tempsurface = SDL_LoadBMP(directory_imagebg);
            imagebg = SDL_CreateTextureFromSurface(renderer, tempsurface);
            SDL_FreeSurface(tempsurface);

            tempsurface = SDL_LoadBMP(directory_images);
            images = SDL_CreateTextureFromSurface(renderer, tempsurface);
            SDL_FreeSurface(tempsurface);

            image1.x = 0; image1.y = 0; image1.w = 300; image1.h = 400;
            image2.x = 300; image2.y = 0; image2.w = 300; image2.h = 400;
            image3.x = 0; image3.y = 400; image3.w = 600; image3.h = 400;

            texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, SCREEN_WIDTH, SCREEN_HEIGHT);

            SDL_SetRenderTarget(renderer, texture);
            SDL_RenderClear(renderer);
            SDL_RenderCopy(renderer, imagebg, NULL, NULL);

            SDL_SetRenderTarget(renderer, NULL);
        }
    }

    return true;
}

void render()
{
    SDL_RenderClear(renderer);

    if (showimagebg)
        SDL_RenderCopy(renderer, imagebg, NULL, NULL);
    if (showimage1)
        SDL_RenderCopy(renderer, images, &image1, &image1loc);
    if (showimage2)
        SDL_RenderCopy(renderer, images, &image2, &image2loc);
    if (showimage3)
        SDL_RenderCopy(renderer, images, &image3, &image3loc);
        
    SDL_RenderPresent(renderer);
}

void update()
{
    //check input and trigger randmomise or quit 
    // Source: https://www.youtube.com/watch?v=Gjhvz4banWA
    SDL_Event event;
    const Uint8* keystates = SDL_GetKeyboardState(NULL);
    while (SDL_PollEvent(&event))
    {
        if (event.type == SDL_QUIT)
            running = false;
        if (keystates[SDL_SCANCODE_0] || keystates[SDL_SCANCODE_KP_0])
        {
            cout << "toggle background\n";
            showimagebg = !showimagebg;
            SDL_Delay(200);
        }
        if (keystates[SDL_SCANCODE_1] || keystates[SDL_SCANCODE_KP_1])
        {
            cout << "toggle image 1\n";
            showimage1 = !showimage1;
            image1loc = randomrect(image1.w, image1.h);
            SDL_Delay(200);
        }
        if (keystates[SDL_SCANCODE_2] || keystates[SDL_SCANCODE_KP_2])
        {
            cout << "toggle image 2\n";
            showimage2 = !showimage2;
            image2loc = randomrect(image2.w, image2.h);
            SDL_Delay(200);
        }
        if (keystates[SDL_SCANCODE_3] || keystates[SDL_SCANCODE_KP_3])
        {
            cout << "toggle image 3\n";
            showimage3 = !showimage3;
            image3loc = randomrect(image3.w, image3.h);
            SDL_Delay(200);
        }
    }
}

void close()
{
    //destroy textures
    SDL_DestroyTexture(imagebg);
    SDL_DestroyTexture(images);
    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(renderer);

    //Destroy window
    SDL_DestroyWindow(window);
    window = NULL;

    //Quit SDL subsystems
    SDL_Quit();
}

int main(int argc, char* args[])
{
    //Start up SDL and create window
    if (!init())
    {
        printf("Failed to initialize!\n");
        return 1;
    }

    while (running)
    {
        render();
        update();
    }

    //Free resources and close SDL
    close();

	return 0;
}