#ifndef ROOM_H
#define ROOM_H

#include "Inventory.h"

class Room
{
private:
	vector<string> SplitString(string s, string delimiter);
public:
	string Name;
	Inventory Inventory;
	vector<Entity*> entities;

	Room();
	Room(string name, string inventorystring);

	//inventory functions
	void PrintInventory();
	void deposit(Item* item);
	Item* withdraw(string ID);
	Item* finditem(string ID);

	Entity* findentity(string ID);
};

#endif