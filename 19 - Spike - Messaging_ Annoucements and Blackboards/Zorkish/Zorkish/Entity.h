#ifndef ENTITIES_H
#define ENTITIES_H

#include <string>
#include <algorithm>
#include <iostream>
#include <cctype>

#include "ComponentManager.h"
//#include "MessageSystem.h"

using namespace std;

class Entity
{
public:
	string name;
	string description;
	string messageid;
	ComponentManager components;

	Entity();
	Entity(string newname, string newdescription, vector<string> componentinstructions);

	string getID();
	string getName();
	string getDescription();

	vector<string> MessageProcess(string senderid, string recieverid, vector<string> data);
};

#endif