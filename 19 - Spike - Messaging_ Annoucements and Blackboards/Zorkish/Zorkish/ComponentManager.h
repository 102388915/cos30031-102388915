#ifndef COMPONENTMANAGER_H
#define COMPONENTMANAGER_H

#include <map>

#include "Components.h"

using namespace std;

class ComponentManager
{
private:
	vector<Component*> components;

public:
	ComponentManager();
	void AddComponent(string componentinstructions);
	void printAttributes();

	//possible components
	bool Attack(int change);
	bool Heal(int change);
	int Damage();
	bool Open();
	bool Close();
	string Unlock(vector<string> key);
	string Key();

	vector<string> MessageProcess(string senderid, string recieverid, vector<string> data);
};

#endif