#include "Commands.h"

void GoCommand::execute(string arg, Graph* graph)
{
	// input = "go ____"
	if (arg.find("north") != string::npos)
		graph->north();
	else if (arg.find("south") != string::npos)
		graph->south();
	else if (arg.find("east") != string::npos)
		graph->east();
	else if (arg.find("west") != string::npos)
		graph->west();
	else
		cout << "error in go command input" << endl;
}

void HelpCommand::execute(string arg, Graph* graph)
{
	// handled in command manager
}

void InventoryCommand::execute(string arg, Graph* graph)
{
	// input = "inventory"

	if (arg.find("inventory") != string::npos)
		graph->player->printInventory();
}

void LookCommand::execute(string arg, Graph* graph)
{
	// input = "look"

	graph->look();
}

void LookAtCommand::execute(string arg, Graph* graph)
{
	// input = "look at ____"

	// isolate what we're looking at
	size_t pos = arg.find(alias + " ");
	if (pos != std::string::npos)
		arg.erase(pos, alias.length()+1);

	// remove number
	if (!isalpha(arg[0]))
		arg.erase(0, 2);

	// remove plural
	if (arg[arg.size()-1] == 's')
		arg.pop_back();
		
	//cout << "trying to find: " << arg << endl;

	if (graph->player->finditem(arg) != nullptr)
	{
		cout << graph->player->finditem(arg)->getDescription() << endl;
		graph->player->finditem(arg)->components.printAttributes();
	}
	else if (graph->finditem(arg) != nullptr)
	{
		cout << graph->finditem(arg)->getDescription() << endl;
		graph->finditem(arg)->components.printAttributes();
	}
	else if (graph->findentity(arg) != nullptr)
	{
		cout << graph->findentity(arg)->getDescription() << endl;
		graph->findentity(arg)->components.printAttributes();
	}
	else
		cout << "error could not find: " << arg << endl;
}

void DebugTreeCommand::execute(string arg, Graph* graph)
{
	// input = "debugtree"

	graph->debug();

	cout << "player coords: " << graph->player->x << "," << graph->player->y << endl;
	graph->player->printInventory();
}

void AttackCommand::execute(string arg, Graph* graph)
{
	// input = "attack ____ with ____"

	//split string
	vector<string> args;
	size_t pos = 0;
	string token;
	while ((pos = arg.find(" ")) != string::npos) {
		token = arg.substr(0, pos);
		args.push_back(token);
		arg.erase(0, pos + 1);
	}
	args.push_back(arg);

	//assume fists
	int damage = 1;

	//handle "with ____"
	if (args.size() >= 4)
	{
		if (args[2] == "with")
		{
			if (graph->player->finditem(args[3]) != nullptr)
			{
				damage = graph->player->finditem(args[3])->components.Damage();
			}
			else
			{
				cout << "you dont have a " << args[3] << endl;
				return;
			}
		}
	}
	
	//process damage
	for (Entity* entity : graph->currentRoom()->entities)
	{
		if (entity->name == (args[1]))
		{
			cout << "you attack the " << entity->name << " and ";
			if (entity->components.Attack(damage))
				break;
			else
				cout << "the attack fails" << endl;
		}
	}

	//the opponent tries to attack back?
}

void TakeCommand::execute(string arg, Graph* graph)
{
	//input = "take ____"
	arg.erase(0, alias.size()+1);
	//cout << "trying to take: " << arg << endl;
	if (graph->finditem(arg) != nullptr)
	{
		graph->player->deposit(graph->withdraw(arg));
		cout << "you took the " << arg << " from the ground" << endl;
	}
}

void OpenCommand::execute(string arg, Graph* graph)
{
	//input = entityid
	//graph->findentity(arg)->components.Open();
	// 
	//this method has been implemented via the message system in command manager
}

void CloseCommand::execute(string arg, Graph* graph)
{
	//input = entityid
	//graph->findentity(arg)->components.Close();
	// 
	//this method has been implemented via the message system in command manager
}

void UnlockCommand::execute(string arg, Graph* graph)
{
	//graph->findentity(arg)->components.Unlock(arg);
	// 
	//this method has been implemented via the message system in command manager
}