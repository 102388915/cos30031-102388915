#include "Components.h"

CombinationLockComponent::CombinationLockComponent(string newcombination)
{
	description = "this box is locked and shut";
	combination = newcombination;
	unlocked = false;
	opened = false;
}

string CombinationLockComponent::Update(vector<string> input)
{
	if (input[0] == "open")
	{
		if (unlocked)
		{
			cout << "you opened the box" << endl;
			opened = true;
			description = "the box contains a brand-name cookie";
		}
		else 
			cout << "the object is locked" << endl;

		return "success";
	}
		
	else if (input[0] == "close")
	{
		if (opened == true)
		{
			cout << "you closed the box" << endl;
			opened = false;
			description = "the box is unlocked but still closed";
		}
		else
			cout << "you cant close a closed box" << endl;

		return "success";
	}

	else if (input[0] == "unlock")
	{
		if (input[1] == combination)
		{
			cout << "the code worked! the box is unlocked!" << endl;
			unlocked = true;
			description = "the box is unlocked but still closed";
		}
		else
			cout << "the combination is wrong" << endl;

		return "success";
	}

	return "";
}