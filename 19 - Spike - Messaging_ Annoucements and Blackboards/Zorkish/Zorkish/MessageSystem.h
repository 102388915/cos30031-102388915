#ifndef MESSAGESYSTEM_H
#define MESSAGESYSTEM_H

#include "Entity.h"

class MessageSystem
{
private:
	map<string, Entity*> entities;

public:
	MessageSystem();

	void AddEntity(Entity* newentity);
	void RemoveEntity(string messageid);
	void Message(string senderid, string recieverid, vector<string> data);
};

#endif