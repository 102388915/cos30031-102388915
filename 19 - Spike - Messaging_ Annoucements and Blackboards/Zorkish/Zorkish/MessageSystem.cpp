#include "MessageSystem.h"

MessageSystem::MessageSystem()
{
}

void MessageSystem::AddEntity(Entity* newentity)
{
	if (newentity != nullptr)
	{
		newentity->messageid = newentity->getID();
		entities[newentity->messageid] = newentity;
	}
}

void MessageSystem::RemoveEntity(string messageid)
{
	entities[messageid] = nullptr;
}

void MessageSystem::Message(string senderid, string recieverid, vector<string> data)
{
	vector<string> reply;

	reply = entities[recieverid]->MessageProcess(senderid, recieverid, data);

	if (!reply.empty())
	{
		vector<string> replydata(reply.begin() + 2, reply.end());
		Message(reply[0], reply[1], replydata);
	}
}
