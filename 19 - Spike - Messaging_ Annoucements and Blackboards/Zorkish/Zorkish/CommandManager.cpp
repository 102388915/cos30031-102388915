#include "CommandManager.h"

vector<string> CommandManager::SplitString(string s, string delimiter)
{
	vector<string> result;
	size_t pos = 0;
	string token;
	while ((pos = s.find(delimiter)) != string::npos) {
		token = s.substr(0, pos);
		result.push_back(token);
		s.erase(0, pos + delimiter.length());
	}
	result.push_back(s);
	return result;
}

void CommandManager::RegisterEntity(string entityid, Graph* graph)
{
	try
	{
		messageSystem.AddEntity(graph->findentity(entityid));
		messageSystem.AddEntity(graph->finditem(entityid));
		messageSystem.AddEntity(graph->player->finditem(entityid));
	}
	catch (exception e) { cout << e.what();	}
}

CommandManager::CommandManager()
{
	messageSystem = MessageSystem();

	commands[CommandType::GO] = new GoCommand();
	commands[CommandType::HELP] = new HelpCommand();
	commands[CommandType::INVENTORY] = new InventoryCommand();
	commands[CommandType::LOOK] = new LookCommand();
	commands[CommandType::LOOKAT] = new LookAtCommand();
	commands[CommandType::DEBUGTREE] = new DebugTreeCommand();
	commands[CommandType::ATTACK] = new AttackCommand();
	commands[CommandType::TAKE] = new TakeCommand();
	commands[CommandType::OPEN] = new OpenCommand();
	commands[CommandType::CLOSE] = new CloseCommand();
	commands[CommandType::UNLOCK] = new UnlockCommand();


	commands[CommandType::GO]->alias = "go";
	commands[CommandType::HELP]->alias = "help";
	commands[CommandType::INVENTORY]->alias = "inventory";
	commands[CommandType::LOOK]->alias = "look";
	commands[CommandType::LOOKAT]->alias = "look at";
	commands[CommandType::DEBUGTREE]->alias = "debugtree";
	commands[CommandType::ATTACK]->alias = "attack";
	commands[CommandType::TAKE]->alias = "take";
	commands[CommandType::OPEN]->alias = "open";
	commands[CommandType::CLOSE]->alias = "close";
	commands[CommandType::UNLOCK]->alias = "unlock";
}

void CommandManager::execute(string arg, Graph* graph)
{
	vector<string> args = SplitString(arg, " ");

	if (args[0] == commands[CommandType::GO]->alias)
		commands[CommandType::GO]->execute(arg, graph);

	if (args[0] == commands[CommandType::HELP]->alias)
	{
		cout << "commands are: " << endl;
		cout << "look at: " << commands[CommandType::LOOKAT]->alias << endl;
		cout << "help: " << commands[CommandType::HELP]->alias << endl;
		cout << "inventory: " << commands[CommandType::INVENTORY]->alias << endl;
		cout << "go: " << commands[CommandType::GO]->alias << endl;
		cout << "look: " << commands[CommandType::LOOK]->alias << endl;
		cout << "alias: " << "alias" << endl;
		cout << "debugtree: " << commands[CommandType::DEBUGTREE]->alias << endl;
	}

	if (args[0] == commands[CommandType::INVENTORY]->alias)
		commands[CommandType::INVENTORY]->execute(arg, graph);

	if (arg.find(commands[CommandType::LOOKAT]->alias) != string::npos)
		commands[CommandType::LOOKAT]->execute(arg, graph);
	else if (args[0] == commands[CommandType::LOOK]->alias)
		commands[CommandType::LOOK]->execute(arg, graph);
	
	if (args[0] == "alias")
	{
		map<CommandType, Command*>::iterator it;
		bool success = false;

		cout << "attempting to reassign alias..." << endl;
		for (it = commands.begin(); it != commands.end(); it++)
		{
			if (it->second->alias == args[1])
			{
				it->second->alias = args[2];
				success = true;
				cout << "function " << args[1] << " set to " << it->second->alias << endl;
				break;
			}
		}

		if (!success) cout << "alias failed" << endl;
	}

	if (args[0] == commands[CommandType::DEBUGTREE]->alias)
		commands[CommandType::DEBUGTREE]->execute(arg, graph);

	if (args[0] == commands[CommandType::ATTACK]->alias)
		commands[CommandType::ATTACK]->execute(arg, graph);

	if (args[0] == commands[CommandType::TAKE]->alias)
		commands[CommandType::TAKE]->execute(arg, graph);

	if (args[0] == commands[CommandType::OPEN]->alias)
	{
		//commands[CommandType::OPEN]->execute(args[1], graph);
		RegisterEntity(args[1], graph);
		messageSystem.Message("command manager", args[1], vector<string>{"open"});
	}
		
	if (args[0] == commands[CommandType::CLOSE]->alias)
	{
		//commands[CommandType::CLOSE]->execute(args[1], graph);
		RegisterEntity(args[1], graph);
		messageSystem.Message("command manager", args[1], vector<string>{"close"});
	}
		
	if (args[0] == commands[CommandType::UNLOCK]->alias)
	{
		//commands[CommandType::UNLOCK]->execute(args[1], graph);
		RegisterEntity(args[1], graph);
		if (args.size() >= 4)
		{
			RegisterEntity(args[3], graph);
			messageSystem.Message(args[3], args[1], vector<string>{"unlock", args[3]});
		}
		else
			cout << "unlock with what?" << endl;
	}
}