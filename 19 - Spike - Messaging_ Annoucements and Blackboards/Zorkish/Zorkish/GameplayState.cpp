#ifndef STATESINCLUDED
#define STATESINCLUDED
#include "States.h"
#endif

GameplayState::GameplayState()
{
	manager = CommandManager();
	player = Player();
	graph = Graph();
	bool started = false;
}

StateType GameplayState::update(string input)
{
	manager.execute(input, &graph);

	if (input == "quit")
	{
		cout << "Your adventure has ended without fame or fortune." << endl;
		return StateType::MAIN_MENU;
	}
	else if (input == "hiscore")
	{
		cout << "You have entered the magic word and will now see the �New High Score� screen." << endl;
		return StateType::NEWHIGHSCORE;
	}

	return StateType::NONE;
}

void GameplayState::render()
{
	cout << endl;
	if (!started)
	{
		//cout << "Welcome to Zorkish: Void World" << endl;
		//cout << "This world is simple and pointless. Used it to test Zorkish phase 1 spec." << endl;
		graph.look();
		started = true;
	}

	cout << graph.directions() << endl;
	cout << "What would you like to do?" << endl;
	
	cout << ":>";
}

bool GameplayState::WorldGen(string world)
{
	try
	{
		ifstream file(world);
		if (file.is_open())
		{
			file.close();

			graph = Graph(&player, world);

			return true;
		}
		else
		{
			cout << "Error: file did not open" << endl;
		}
	}
	catch (exception e)
	{
		cout << e.what();
	}
	return false;
}
