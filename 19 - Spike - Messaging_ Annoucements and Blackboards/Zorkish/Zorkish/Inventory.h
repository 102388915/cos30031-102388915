#ifndef INVENTORY_H
#define INVENTORY_H

#include <vector>

#include "Items.h"

class Inventory
{
private:
	vector<Item*> items;
public:
	Inventory();
	~Inventory();
	void print();
	bool empty();
	void deposit(Item* item);
	Item* withdraw(string ID);
	Item* finditem(string ID);
};

#endif