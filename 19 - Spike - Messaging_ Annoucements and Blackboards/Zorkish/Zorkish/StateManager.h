#ifndef STATEMANAGER_H
#define STATEMANAGER_H

#include "States.h"

class StateManager
{
private:
	map <StateType, State*> states;
	State* currentState;

public:
	StateManager();
	void update(string input);
	void render();

	bool isRunning;
};

#endif