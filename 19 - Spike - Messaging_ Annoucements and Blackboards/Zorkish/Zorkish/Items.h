#ifndef ITEMS_H
#define ITEMS_H

#include "Entity.h"

using namespace std;

class Item : public Entity
{
public:
	int quantity;

	Item();
	Item(string newname, string newdescription, int newquantity, vector<string> components);

	string getName();
	int getQuantity();
	void setQuantity(int newQuantity);

	void lookAt();
	void lookIn();
	void open();
	void close();
};

#endif