#include "Components.h"

KeyLockComponent::KeyLockComponent(string newkey)
{
	key = newkey;
	unlocked = false;
	opened = false;
}

string KeyLockComponent::Update(vector<string> input)
{
	if (input[0] == "open")
	{
		if (unlocked)
		{
			cout << "you opened the box" << endl;
			opened = true;
			description = "the box contains a premium cookie";
		}
		else
			cout << "the object is locked" << endl;

		return "success";
	}

	else if (input[0] == "close")
	{
		if (opened == true)
		{
			cout << "you closed the box" << endl;
			opened = false;
			description = "the box is unlocked but still closed";
		}
		else
			cout << "you cant close a closed box" << endl;

		return "success";
	}

	else if (input[0] == "unlock")
	{
		//if input.size > 2, then attempt input[2] as the key
		//else reply with a request for the key from message system
		if (input.size() > 2)
		{
			if (input[2] == key)
			{
				cout << "the key worked! the box is unlocked!" << endl;
				unlocked = true;
				description = "the box is unlocked but still closed";
			}
			else
				cout << "the key doesnt fit" << endl;
		}
		else
			return "key";

		return "success";
	}

	return "";
}