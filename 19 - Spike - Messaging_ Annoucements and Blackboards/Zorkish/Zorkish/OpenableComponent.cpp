#include "Components.h"

OpenableComponent::OpenableComponent()
{
	description = "this object can be opened";
	opened = false;
}

string OpenableComponent::Update(vector<string> input)
{
	if (input[0] == "open")
	{
		cout << "you opened the box" << endl;
		opened = true;
		description = "inside the box is a disgusting cookie";
	}
	if (input[0] == "close")
	{
		cout << "you closed the box" << endl;
		opened = false;
		description = "this object can be opened";
	}
	return "";
}