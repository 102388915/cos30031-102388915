#include "Graph.h"

vector<string> Graph::SplitString(string s, string delimiter)
{
    vector<string> result;
    size_t pos = 0;
    string token;
    while ((pos = s.find(delimiter)) != string::npos) {
        token = s.substr(0, pos);
        result.push_back(token);
        s.erase(0, pos + delimiter.length());
    }
    result.push_back(s);
    return result;
}

Graph::Graph()
{
    player = nullptr;
}

Graph::Graph(Player* newplayer, string filelocation)
{
    player = newplayer;

    //load map file
    ifstream file(filelocation);
    if (file.is_open())
    {
        string line;

        //player location
        getline(file, line);
        vector<string> instructions = SplitString(line, ";");
        player->setcoords(stoi(instructions[0]), stoi(instructions[1]));

        //rooms
        while (getline(file, line))
        {
            instructions = SplitString(line, ";");
            int x = stoi(instructions[0]);
            int y = stoi(instructions[1]);
            string name = instructions[2];
            string entities = instructions[3];

            Room* room = new Room(name, entities);

            M.insert(make_pair(x, map<int, Room>()));
            M[x].insert(make_pair(y, *room));
        }
        file.close();
    }
    else
        cout << "failed to read file" << endl;
}

void Graph::north()
{
    if (M[player->x][player->y - 1].Name != "")
    {
        player->y -= 1;
        cout << "You move north" << endl;
    }
    else
        cout << "You can't move north" << endl;
}

void Graph::south()
{
    if (M[player->x][player->y + 1].Name != "")
    {
        player->y += 1;
        cout << "You move south" << endl;
    }
    else
        cout << "You can't move south" << endl;
}

void Graph::east()
{
    if (M[player->x + 1][player->y].Name != "")
    {
        player->x += 1;
        cout << "You move east" << endl;
    }
    else
        cout << "You can't move east" << endl;
}

void Graph::west()
{
    if (M[player->x - 1][player->y].Name != "")
    {
        player->x -= 1;
        cout << "You move west" << endl;
    }
    else
        cout << "You can't move west" << endl;
}

void Graph::look()
{
    cout << "You are in the " << M[player->x][player->y].Name << endl;

    //items
    cout << "Items: ";
    M[player->x][player->y].PrintInventory();

    //entities
    cout << "Entities: ";
    string foundentities = "";
    for (Entity* e : currentRoom()->entities)
        foundentities += e->getName() + ", ";
    if (foundentities != "")
    {
        foundentities.pop_back();
        foundentities.pop_back();
        cout << foundentities << endl;
    }
    else
    {
        cout << "None" << endl;
    }
        
}

void Graph::debug()
{
    map<int, map<int, Room>>::iterator it1; // first layer

    for (it1 = M.begin(); it1 != M.end(); it1++)
    {
        map<int, Room>::iterator it2; // second layer

        for (it2 = it1->second.begin(); it2 != it1->second.end(); it2++)
        {
            if (it2->second.Name != "") // prevents crash on accessing inventory
            {
                cout << it1->first << "," << it2->first << endl;
                cout << "Room name: " << it2->second.Name << endl;
                it2->second.PrintInventory();
            }
        }
    }
}

string Graph::directions()
{
    string result = "You can move: ";
    if (M[player->x][player->y - 1].Name != "")
        result += "North, ";
    if (M[player->x][player->y + 1].Name != "")
        result += "South, ";
    if (M[player->x + 1][player->y].Name != "")
        result += "East, ";
    if (M[player->x - 1][player->y].Name != "")
        result += "West, ";

    //delete ", "
    result.pop_back();
    result.pop_back();

    if (result == "You can move")
        result = "There is nowhere you can move...";

    return result;
}

Room* Graph::currentRoom()
{
    return &M[player->x][player->y];
}

void Graph::deposit(Item* item)
{
    M[player->x][player->y].deposit(item);
}

Item* Graph::withdraw(string ID)
{
    return M[player->x][player->y].withdraw(ID);
}

Item* Graph::finditem(string ID)
{
    return M[player->x][player->y].finditem(ID);
}

Entity* Graph::findentity(string ID)
{
    return M[player->x][player->y].findentity(ID);
}
