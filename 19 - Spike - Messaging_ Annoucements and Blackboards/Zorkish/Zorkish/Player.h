#ifndef PLAYER_H
#define PLAYER_H

#include "Inventory.h"

class Player
{
private:
	Inventory inventory;
	int score;
public:
	Player();

	//location functions
	void setcoords(int newx, int newy);
	int x;
	int y;

	//inventory functions
	void printInventory();
	void deposit(Item* item);
	Item* withdraw(string ID);
	Item* finditem(string ID);

	//score functions
	//void addscore();
	//virtual void removescore();
};

#endif