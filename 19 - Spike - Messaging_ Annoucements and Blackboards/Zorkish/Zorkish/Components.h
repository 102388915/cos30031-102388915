#ifndef COMPONENTS_H
#define COMPONENTS_H

#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Component
{
public:
	string id;
	string description;

	virtual string Update(vector<string> input) = 0;
};

class HealthComponent : public Component
{
private:
	int currentHealth;
	int maxHealth;

	void AddHealth(int change);
	void SubtractHealth(int change);
	int HealthValue();
public:
	HealthComponent(int health);

	string Update(vector<string> input);
};

class DamageComponent : public Component
{
private:
	int damage;

public:
	DamageComponent(int power);

	string Update(vector<string> input);
};

class OpenableComponent : public Component
{
private:
	bool opened;

public:
	OpenableComponent();

	string Update(vector<string> input);
};

class CombinationLockComponent : public Component
{
private:
	string combination;

	bool unlocked;
	bool opened;

public:
	CombinationLockComponent(string newcombination);

	string Update(vector<string> input);
};

class KeyComponent : public Component
{
private:
	string key;
public:
	KeyComponent(string newkey);

	string Update(vector<string> input);
};

class KeyLockComponent : public Component
{
private:
	string key;

	bool unlocked;
	bool opened;
public:
	KeyLockComponent(string newkey);

	string Update(vector<string> input);
};

#endif