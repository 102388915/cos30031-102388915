#include "Entity.h"

Entity::Entity()
{
    name = "";
    description = "";
	messageid = getID();
}

Entity::Entity(string newname, string newdescription, vector<string> componentinstructions)
{
    //process instructions to build name, description and attributes
    name = newname;
    description = newdescription;
	messageid = getID();

    for (string c : componentinstructions)
        components.AddComponent(c);
}

string Entity::getID()
{
	//same as get name without capitals or quantity specifier
	string id = name;

	std::transform(id.begin(), id.end(), id.begin(),
		[](unsigned char c) { return std::tolower(c); });

	return id;
}

string Entity::getName()
{
	return name;
}

string Entity::getDescription()
{
	if (description != "")
		return description;
	return "There is no description for this object";
}

vector<string> Entity::MessageProcess(string senderid, string recieverid, vector<string> data)
{
	vector<string> result;

	if (recieverid == messageid)
	{
		result = components.MessageProcess(senderid, recieverid, data);
	}

	return result;
}
