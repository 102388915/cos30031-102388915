#include "Items.h"

Item::Item()
{
	name = "";
	description = "";
	messageid = getID();

	quantity = 1;
}

Item::Item(string newname, string newdescription, int newquantity, vector<string> newcomponents)
{
	name = newname;
	description = newdescription;
	messageid = getID();

	quantity = newquantity;

	for (string c : newcomponents)
		components.AddComponent(c);
}

string Item::getName()
{
	if (quantity > 1)
		return (to_string(quantity) + " " + name + "s");
	return name;
}

int Item::getQuantity()
{
	return quantity;
}

void Item::setQuantity(int newQuantity)
{
	quantity = newQuantity;
}

void Item::lookAt()
{
	cout << description << endl;
}

void Item::lookIn()
{
	// fails unless it has its own inventory
	cout << "you cannot look in the " << name << endl;
}

void Item::open()
{
	// fails unless it can open
	cout << "you cannot open the " << name << endl;
}

void Item::close()
{
	// fails unless it can open
	cout << "you cannot close the " << name << endl;
}