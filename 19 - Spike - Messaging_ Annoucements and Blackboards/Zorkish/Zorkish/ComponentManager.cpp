#include "ComponentManager.h"

ComponentManager::ComponentManager()
{
}

void ComponentManager::AddComponent(string componentinstructions)
{
    if (componentinstructions.find("health") != string::npos)
    {
        componentinstructions.erase(0, 6);
        components.push_back(new HealthComponent(stoi(componentinstructions)));
    }
    else if (componentinstructions.find("damage") != string::npos)
    {
        componentinstructions.erase(0, 6);
        components.push_back(new DamageComponent(stoi(componentinstructions)));
    }
    else if (componentinstructions.find("openable") != string::npos)
    {
        components.push_back(new OpenableComponent());
    }
    else if (componentinstructions.find("combinationlock") != string::npos)
    {
        componentinstructions.erase(0, 15);
        components.push_back(new CombinationLockComponent(componentinstructions));
    }
    else if (componentinstructions.find("keylock") != string::npos)
    {
        componentinstructions.erase(0, 7);
        components.push_back(new KeyLockComponent(componentinstructions));
    }
    else if (componentinstructions.find("key") != string::npos)
    {
        componentinstructions.erase(0, 3);
        components.push_back(new KeyComponent(componentinstructions));
    }
}

void ComponentManager::printAttributes()
{
    for (Component* c : components)
    {
        cout << c->description << endl;
    }
}

bool ComponentManager::Attack(int change)
{
    //returns "" if failed
    //returns "success" if processed and complete

    for (Component* c : components)
    {
        if (c->Update(vector<string> {"subtracthealth", to_string(change)}) != "")
            return true;
    }
    return false;
}

bool ComponentManager::Heal(int change)
{
    //returns "" if failed
    //returns "success" if processed and complete

    for (Component* c : components)
    {
        if (c->Update(vector<string> {"addhealth", to_string(change)}) != "")
            return true;
    }
    return false;
}

int ComponentManager::Damage()
{
    ////returns "" if failed
    //returns [value] if processed and complete

    string result;
    for (Component* c : components)
    {
        result = c->Update(vector<string> {"damage"});
        if (result != "")
            return stoi(result);
    }
    return NULL;
}

bool ComponentManager::Open()
{
    //returns "" if failed
    //returns "success" if processed and complete

    for (Component* c : components)
    {
        if (c->Update(vector<string> {"open"}) != "")
            return true;
    }
    return false;
}

bool ComponentManager::Close()
{
    //returns "" if failed
    //returns "success" if processed and complete

    for (Component* c : components)
    {
        if (c->Update(vector<string> {"close"}) != "")
            return true;
    }
    return false;
}

string ComponentManager::Unlock(vector<string> data)
{
    //returns "" if failed
    //returns "success" if processed and complete
    //returns "delete" in response to a used key
    //returns "key" in response to ask for a key.code

    string result;

    for (Component* c : components)
    {
        result = c->Update(data);

        // result = success if lock found and done, else returns reply message
        if (result != "")
            return result;
    }

    return "";
}

string ComponentManager::Key()
{
    string result;

    for (Component* c : components)
    {
        result = c->Update(vector<string> {"key"});

        // result = success if lock found and done, else returns reply message
        if (result != "")
            return result;
    }

    return "";
}

vector<string> ComponentManager::MessageProcess(string senderid, string recieverid, vector<string> data)
{
    vector<string> result;
    string message;

    if (data[0] == "open")
        Open();
    if (data[0] == "close")
        Close();
    if (data[0] == "unlock")
    {
        message = Unlock(data);
        if (message == "key")
            result = { recieverid, senderid, message };
    }
    if (data[0] == "key")
    {
        result = { recieverid, senderid, "unlock", "key", Key() };
    }

    return result;
}
