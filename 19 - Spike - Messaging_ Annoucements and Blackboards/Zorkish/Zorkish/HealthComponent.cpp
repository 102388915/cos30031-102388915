#include "Components.h"

HealthComponent::HealthComponent(int health)
{
	description = "This Object Has Health";
	maxHealth = health;
	currentHealth = health;
}

string HealthComponent::Update(vector<string> input)
{
	if (input[0] == "get")
	{
		if (input[1] == "currentHealth")
			return to_string(currentHealth);
		if (input[1] == "maxHealth")
			return to_string(maxHealth);
	}
	else if (input[0] == "addhealth")
	{
		AddHealth(stoi(input[1]));
		return "success";
	}
	else if (input[0] == "subtracthealth")
	{
		SubtractHealth(stoi(input[1]));
		return "success";
	}

	return "";
}

void HealthComponent::AddHealth(int change)
{
	currentHealth += change;
	if (currentHealth > maxHealth)
		currentHealth = maxHealth;
}

void HealthComponent::SubtractHealth(int change)
{
	cout << "HIT for " << change << " damage" << endl;
	currentHealth -= change;
	if (currentHealth <= 0)
	{
		cout << "the creature has gone unconcious..." << endl;
		currentHealth = 0;
	}
}

int HealthComponent::HealthValue()
{
	return currentHealth;
}