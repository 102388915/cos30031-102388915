#ifndef COMMANDMANAGER_H
#define COMMANDMANAGER_H

#include <map>
#include <string>

#include "Commands.h"
#include "MessageSystem.h"

using namespace std;

class CommandManager
{
private:
	MessageSystem messageSystem;
	map <CommandType, Command*> commands;

	vector<string> SplitString(string s, string delimiter);
public:
	CommandManager();

	void RegisterEntity(string entityid, Graph* graph);
	void execute(string arg, Graph* graph);
};

#endif