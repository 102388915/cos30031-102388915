#include "Components.h"

KeyComponent::KeyComponent(string newkey)
{
	key = newkey;
}

string KeyComponent::Update(vector<string> input)
{
	if (input[0] == "key")
		return key;
	return "";
}
