#include "Components.h"

DamageComponent::DamageComponent(int power)
{
	description = "This object could do some damage";
	damage = power;
}

string DamageComponent::Update(vector<string> input)
{
	if (input[0] == "damage")
		return to_string(damage);
	else
		return "";
}